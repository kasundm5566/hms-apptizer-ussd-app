package io.apptizer.tap.ussd.core.menus.services;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.Map;

/**
 * Created by dinesh on 5/12/17.
 */
@Component
public class UssdContentRenderer {

    private VelocityEngine initializeVelocityEngine() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        return velocityEngine;
    }

    public String renderStaticMenu(String caption, Map<Integer, String> items) {
        VelocityEngine velocityEngine = initializeVelocityEngine();
        Template template = velocityEngine.getTemplate("/velocity_templates/static_menu.vm");
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("context_caption", caption);
        velocityContext.put("context_items", items);
        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);
        return stringWriter.toString();
    }

    public String renderFormMenu(String caption) {
        VelocityEngine velocityEngine = initializeVelocityEngine();
        Template template = velocityEngine.getTemplate("/velocity_templates/form_menu.vm");
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("context_caption", caption);
        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);
        return stringWriter.toString();
    }
}
