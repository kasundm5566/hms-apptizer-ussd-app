package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.product.AddOn;
import io.apptizer.api.model.product.VariantType;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.ProductService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/22/17.
 */
@Controller
public class VariantsMenuTemplate extends AbstractMenuTemplate {

    private final ProductService productService;
    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public VariantsMenuTemplate(ProductService productService,
                                MenuResolver menuResolver,
                                MessageReader messageReader,
                                UssdContentRenderer ussdContentRenderer) {
        super("variantsMenu", menuResolver);
        this.productService = productService;
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        if (!session.getContext().containsKey(KeyBox.VARIANT_ID + input)) {
            return menuResolver.getMenu("invalidInputMenu");
        } else {
            session.getContext().put(KeyBox.SELECTED_VARIANT_ID, session.getContext().get(KeyBox.VARIANT_ID + input));
            String productId = session.getContext().get(KeyBox.SELECTED_PRODUCT_ID);
            List<AddOn> addOnList = productService.getProductAddOns(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), productId);
            if (addOnList.size() > 0) {
                session.getContext().put(KeyBox.ADDON_ITEM_NO, "0");
                return menuResolver.getMenu("addOnSelectMenu");
            }
            return menuResolver.getMenu("quantityMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String productId = session.getContext().get(KeyBox.SELECTED_PRODUCT_ID);
        List<VariantType> variantTypes = productService.getProductVariants(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), productId);

        String menuCaption = messageReader.readMessage("select.variant", new Object[]{});
        Map<Integer, String> menuOptions = new HashMap<>();

        for (int i = 0; i < variantTypes.size(); i++) {
            VariantType type = variantTypes.get(i);
            String size = type.getName();
            Float price = type.getPrice().getAmount();
            String currencyCode = type.getPrice().getCurrency().getCode();
            menuOptions.put(i + 1, size + " (" + currencyCode + " " + String.format("%.2f", price) + ")");
            session.getContext().put(KeyBox.VARIANT_ID + (i + 1), type.getSku());
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(menuCaption);
        mtContent.setItems(menuOptions);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "VariantsMenu";
    }
}
