package io.apptizer.tap.ussd.domain;

/**
 * Created by sajith on 2/20/17.
 */
public class UssdMtResponse extends UssdMoResponse {

    private String timeStamp;
    private String requestId;
    private String version;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
