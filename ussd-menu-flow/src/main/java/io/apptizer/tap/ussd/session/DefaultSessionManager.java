package io.apptizer.tap.ussd.session;

import io.apptizer.tap.ussd.menu.MenuManager;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMoType;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by isuru on 3/15/2017.
 */
@Component
public class DefaultSessionManager implements SessionManager {

    private final SessionRepoService sessionRepoService;
    private final MenuManager menuManager;
    private final SessionManagerConfigs configs;
    private final MenuTemplate welcome;
    private final Logger logger = LogManager.getLogger(DefaultSessionManager.class);

    @Autowired
    DefaultSessionManager(SessionRepoService sessionRepoService,
                          MenuManager menuManager,
                          SessionManagerConfigs configs,
                          @Qualifier("welcome") MenuTemplate welcome) {
        this.sessionRepoService = sessionRepoService;
        this.menuManager = menuManager;
        this.configs = configs;
        this.welcome = welcome;
    }

    @Override
    public UssdResponse view(String appId, String msisdn, String input, UssdMoType moType, String sessionId) {
        if (UssdMoType.MO_INIT == moType) {
            String regex = configs.getServiceCode();
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(input);
            Session session = sessionRepoService.create(appId, msisdn);
            if (matcher.matches()) {
                session.getMenuTemplateSequence().push(welcome);
                String option = matcher.group("index");
                session.getContext().put("businessOption", configs.getBusinessIdPrefix() + option);
                return session.getMenuTemplateSequence().peek().content(session);
            } else {
                return session.getMenuTemplateSequence().peek().content(session);
            }
        }

        Optional<Session> sessionOptional = sessionRepoService.lookup(msisdn);

        if (!sessionOptional.isPresent()) {
            return menuManager.getSessionExpiredMenuTemplate().content(new Session(appId, msisdn));
        }

        Session session = sessionOptional.get();

        Stack<MenuTemplate> menuTemplateSequence = session.getMenuTemplateSequence();

        String menuNavs = "Menu: ";
        for (int i = 0; i < menuTemplateSequence.size(); i++) {
            menuNavs += menuTemplateSequence.elementAt(i).toString() + "\\";
        }
        ThreadContext.put("destinationAddress", msisdn);
        ThreadContext.put("sessionId", sessionId);
        logger.info(menuNavs);

        if (configs.getBackInput().equals(input)) {
            if (menuTemplateSequence.peek().isBackSupported()) {
                menuTemplateSequence.pop();
                if (menuTemplateSequence.isEmpty()) {
                    return menuManager.getIndexMenuTemplate().content(session);
                } else {
                    MenuTemplate menuTemplate = menuTemplateSequence.peek();
                    return menuTemplate.content(session);
                }
            } else {
                menuTemplateSequence.push(menuManager.getInvalidInputMenuTemplate());
                return menuManager.getInvalidInputMenuTemplate().content(session);
            }
        }

        if (configs.getExitInput().equals(input)) {
            MenuTemplate exitMenuTemplate = menuManager.getExitMenuTemplate();
            sessionRepoService.invalidate(msisdn);
            return exitMenuTemplate.content(session);
        }

        MenuTemplate menuTemplate = menuTemplateSequence.peek().next(input, session);
        session.getMenuTemplateSequence().push(menuTemplate);
        sessionRepoService.upsert(msisdn, session);
        UssdResponse content = menuTemplate.content(session);
        if (content.getMtType() == UssdMtType.MT_FIN) {
            sessionRepoService.invalidate(msisdn);
        }
        return content;
    }

    @Override
    public MenuTemplate back(Session session) {
        Stack<MenuTemplate> menuTemplateSequence = session.getMenuTemplateSequence();
        menuTemplateSequence.pop();
        if (menuTemplateSequence.isEmpty()) {
            return menuManager.getIndexMenuTemplate();
        } else {
            MenuTemplate menuTemplate = menuTemplateSequence.peek();
            return menuTemplate;
        }
    }
}
