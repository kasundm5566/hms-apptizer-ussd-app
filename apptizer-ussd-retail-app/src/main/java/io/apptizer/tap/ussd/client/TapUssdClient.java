package io.apptizer.tap.ussd.client;

import io.apptizer.tap.ussd.config.TapApiConfigurations;
import io.apptizer.tap.ussd.domain.UssdMtRequest;
import io.apptizer.tap.ussd.domain.UssdMtResponse;
import io.apptizer.tap.ussd.interceptors.RestTemplateInterceptor;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by isuru on 3/15/2017.
 */
@Component
public class TapUssdClient {

    private final TapApiConfigurations tapApiConfigurations;
    private final RestTemplate restTemplate;
    private final RestTemplateInterceptor restTemplateInterceptor;

    @Autowired
    public TapUssdClient(RestTemplateBuilder restTemplateBuilder,
                         TapApiConfigurations tapApiConfigurations,
                         RestTemplateInterceptor restTemplateInterceptor) {
        this.tapApiConfigurations = tapApiConfigurations;
        this.restTemplateInterceptor = restTemplateInterceptor;
        restTemplate = restTemplateBuilder.build();
    }

    public UssdMtResponse sendMt(UssdMtRequest request) {
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(restTemplateInterceptor);
        ThreadContext.put("destinationAddress",request.getDestinationAddress());
        ThreadContext.put("sessionId",request.getSessionId());
        restTemplate.setInterceptors(interceptors);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON);
        return restTemplate.
                postForEntity(tapApiConfigurations.getUssdMtUrl(),
                        new HttpEntity<>(request, headers),
                        UssdMtResponse.class).
                getBody();
    }
}
