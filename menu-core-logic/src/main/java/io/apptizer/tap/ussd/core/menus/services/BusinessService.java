package io.apptizer.tap.ussd.core.menus.services;

import io.apptizer.api.model.merchant.BusinessSummary;
import io.apptizer.api.model.merchant.BusinessSummaryList;
import io.apptizer.tap.ussd.core.menus.config.ApiConfigurations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by dinesh on 3/21/17.
 */
@Component
public class BusinessService {
    private final ApiConfigurations apiConfigurations;
    private final RestTemplate restTemplate;

    @Autowired
    public BusinessService(ApiConfigurations apiConfigurations,
                           RestTemplateBuilder restTemplateBuilder) {
        this.apiConfigurations = apiConfigurations;
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Retrieve the details about the business.
     *
     * @param businessId businessId Business identification no (APP Id).
     * @return BusinessSummary object contains the business data.
     */
    public BusinessSummary getBusinessData(String businessId) {
        // Resource access url
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getBusinessDetailsUrl();
        BusinessSummary businessSummary = restTemplate.getForObject(url, BusinessSummary.class);
        return businessSummary;
    }


    /**
     * Retrieve all the business using the API.
     *
     * @return List of BusinessSummary objects containing the business data.
     */
    public List<BusinessSummary> getAllBusinessesTap() {
        String url = apiConfigurations.getBaseUrlTap() + apiConfigurations.getAllBusinessDetailsUrl();
        BusinessSummaryList businessSummaryList = restTemplate.getForObject(url, BusinessSummaryList.class);
        return businessSummaryList.getBusinessSummaries();
    }

    /**
     * Check whether the business is active or expired.
     *
     * @param businessId businessId Business identification no (APP Id).
     * @return Status code of the process.
     */
    public int checkBusinessStatus(String businessId) {
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getBusinessDetailsUrl();
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        int statusCode = 200;
        try {
            restTemplate.exchange(url, HttpMethod.GET, httpEntity, BusinessSummary.class);
        } catch (HttpClientErrorException exception) {
            statusCode = exception.getStatusCode().value();
        }
        return statusCode;
    }
}
