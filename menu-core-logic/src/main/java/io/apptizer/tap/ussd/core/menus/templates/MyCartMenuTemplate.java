package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.purchase.PurchaseEntry;
import io.apptizer.api.model.purchase.ShoppingCart;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.PurchaseService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/21/17.
 */
@Component
public class MyCartMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final PurchaseService purchaseService;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public MyCartMenuTemplate(MenuResolver menuResolver,
                              PurchaseService purchaseService,
                              MessageReader messageReader,
                              UssdContentRenderer ussdContentRenderer) {
        super("myCartMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.purchaseService = purchaseService;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        switch (input) {
            case "1":
                return menuResolver.getMenu("pickupDateMenu");
            default:
                return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        ShoppingCart shoppingCart = purchaseService.getCartDetails(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), session.getMobileNumber());
        String message = messageReader.readMessage("cart.title", new Object[]{});
        Map<Integer, String> menuOptions = new HashMap<>();
        if (shoppingCart != null) {
            List<PurchaseEntry> purchaseEntries = shoppingCart.getPurchases();
            for (int i = 0; i < purchaseEntries.size(); i++) {
                message += purchaseEntries.get(i).getProductName() + "<br/>";
            }
            menuOptions.put(1, messageReader.readMessage("mycart.checkout.option", new Object[]{}));
        } else {
            message += messageReader.readMessage("empty.cart", new Object[]{});
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(message);
        mtContent.setItems(menuOptions);

        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "MyCartMenu";
    }
}
