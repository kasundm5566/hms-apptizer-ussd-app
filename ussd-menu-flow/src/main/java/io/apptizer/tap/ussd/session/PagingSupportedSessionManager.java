package io.apptizer.tap.ussd.session;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMoType;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.util.MtContentBufferUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by dinesh on 7/21/17.
 */
@Component
@Primary
public class PagingSupportedSessionManager implements SessionManager {

    private final DefaultSessionManager defaultSessionManager;
    private final Cache<String, MtContentBuffer> contentBufferCache;
    private final MtContentBufferUtil mtContentBufferUtil;
    private final SessionManagerConfigs sessionManagerConfigs;

    public static class MtContentBuffer {
        private UssdResponse.MtContent mtContent;
        private int captionReadIndex;
        private int contentItemReadCount;
        private boolean completed;

        public UssdResponse.MtContent getMtContent() {
            return mtContent;
        }

        public void setMtContent(UssdResponse.MtContent mtContent) {
            this.mtContent = mtContent;
        }

        public int getCaptionReadIndex() {
            return captionReadIndex;
        }

        public void setCaptionReadIndex(int captionReadIndex) {
            this.captionReadIndex = captionReadIndex;
        }

        public int getContentItemReadCount() {
            return contentItemReadCount;
        }

        public void setContentItemReadCount(int contentItemReadCount) {
            this.contentItemReadCount = contentItemReadCount;
        }

        public boolean isCompleted() {
            return completed;
        }

        public void setCompleted(boolean completed) {
            this.completed = completed;
        }
    }

    @Autowired
    public PagingSupportedSessionManager(DefaultSessionManager defaultSessionManager,
                                         SessionManagerConfigs sessionManagerConfigs) {
        this.defaultSessionManager = defaultSessionManager;
        this.sessionManagerConfigs = sessionManagerConfigs;
        this.mtContentBufferUtil = new MtContentBufferUtil(sessionManagerConfigs.getMsgChunkSize());
        this.contentBufferCache = CacheBuilder.
                newBuilder().
                expireAfterAccess(300, TimeUnit.SECONDS).
                build();
    }

    @Override
    public UssdResponse view(String appId, String msisdn, String input, UssdMoType moType, String sessionId) {
        if (moType == UssdMoType.MO_INIT) {
            contentBufferCache.invalidate(sessionId);
        }
        if (sessionManagerConfigs.getPaggingButton().equals(input)) {
            try {
                MtContentBuffer mtContentbuffer = contentBufferCache.getIfPresent(sessionId);
                UssdResponse.MtContent content = mtContentBufferUtil.content(mtContentbuffer);
                //TODO: Check here to pagination logic to send invalid menu
                /*if(Strings.isNullOrEmpty(content.getCaption()) && content.getItems().size()==0){

                }*/
                return new UssdResponse(content, UssdMtType.MT_CONT, UssdResponseType.STATIC, !mtContentbuffer.isCompleted());
            } catch (Exception e) {
            }
        }

        UssdResponse ussdResponse = defaultSessionManager.view(appId, msisdn, input, moType, sessionId);
        MtContentBuffer mtContentBuffer = mtContentBufferUtil.buffer(ussdResponse.getContent());
        contentBufferCache.put(sessionId, mtContentBuffer);

        UssdResponse.MtContent content = mtContentBufferUtil.content(mtContentBuffer);

        UssdResponse resp=new UssdResponse(content, UssdMtType.MT_CONT, ussdResponse.getResponseType(), !mtContentBuffer.isCompleted());
        if(ussdResponse.getContent().getCaption().equals(content.getCaption()) && ussdResponse.getContent().getItems().size()==0){
            resp.setHasMorePage(false);
        }
        return resp;
//        return new UssdResponse(content, UssdMtType.MT_CONT, ussdResponse.getResponseType(), !mtContentBuffer.isCompleted());
    }

    @Override
    public MenuTemplate back(Session session) {
        return defaultSessionManager.back(session);
    }
}
