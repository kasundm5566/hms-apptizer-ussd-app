package io.apptizer.tap.ussd.session;

import io.apptizer.tap.ussd.menu.MenuTemplate;

import java.util.Map;
import java.util.Stack;

/**
 * Created by isuru on 3/15/2017.
 */
public class Session {

    private final String mobileNumber;
    private final String tapAppId;
    private Stack<MenuTemplate> menuTemplateSequence;
    private Map<String, String> context;

    public Session(String tapAppId, String mobileNumber) {
        this.mobileNumber = mobileNumber;
        this.tapAppId = tapAppId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getTapAppId() {
        return tapAppId;
    }

    public Stack<MenuTemplate> getMenuTemplateSequence() {
        return menuTemplateSequence;
    }

    public void setMenuTemplateSequence(Stack<MenuTemplate> menuTemplateSequence) {
        this.menuTemplateSequence = menuTemplateSequence;
    }

    public Map<String, String> getContext() {
        return context;
    }

    public void setContext(Map<String, String> context) {
        this.context = context;
    }
}
