package io.apptizer.tap.ussd.util;

import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.session.PagingSupportedSessionManager;

import java.util.*;

/**
 * Created by dinesh on 7/24/17.
 */
public class MtContentBufferUtil {

    public MtContentBufferUtil(int maxContentLength) {
        this.maxContentLength = maxContentLength;
    }

    public static class BufferEmptyException extends  RuntimeException{
    }

    private static final BufferEmptyException bufferEmptyException = new BufferEmptyException();

    private final int maxContentLength;

    public UssdResponse.MtContent content(PagingSupportedSessionManager.MtContentBuffer mtContentBuffer) {
        UssdResponse.MtContent pagedMtContent = new UssdResponse.MtContent();
        Map<Integer, String> itemMap = new TreeMap<>();
        pagedMtContent.setItems(itemMap);
        String caption = mtContentBuffer.getMtContent().getCaption();
        int captionReadIndex = mtContentBuffer.getCaptionReadIndex();
        boolean isCaptionReadComplete =  (caption.length() <= captionReadIndex + 1);
        int remainingCharsForItems = maxContentLength;
        if(isCaptionReadComplete) {
            pagedMtContent.setCaption("");
        } else {
            int remainingCaptionLength = caption.length() - captionReadIndex;
            int readAhead = Integer.min(remainingCaptionLength, maxContentLength);

            String captionNextCharBatch = caption.substring(captionReadIndex, captionReadIndex + readAhead);
            pagedMtContent.setCaption(captionNextCharBatch);
            captionReadIndex = captionReadIndex + captionNextCharBatch.length();
            mtContentBuffer.setCaptionReadIndex(captionReadIndex);

            remainingCharsForItems = remainingCharsForItems - captionNextCharBatch.length();
        }

        int contentItemReadCount = mtContentBuffer.getContentItemReadCount();
        if(remainingCharsForItems > 0) {
            Map<Integer, String> items = new TreeMap<>(mtContentBuffer.getMtContent().getItems());
            if(items != null && !items.isEmpty()) {
                Set<Integer> set = items.keySet();
                Iterator<Integer> iterator = set.iterator();

                int skipped = 0;
                while (contentItemReadCount > skipped) {
                    skipped++;
                    iterator.next();
                }

                while (iterator.hasNext()) {
                    Integer integer = iterator.next();
                    String msg = items.get(integer);
                    int itemString = String.valueOf(integer).length() + msg.length();
                    if(remainingCharsForItems >= itemString) {
                        itemMap.put(integer, msg);
                        remainingCharsForItems = remainingCharsForItems - itemString;
                        contentItemReadCount = contentItemReadCount + 1;
                    } else {
                        break;
                    }
                }

                mtContentBuffer.setContentItemReadCount(contentItemReadCount);
                if(contentItemReadCount == mtContentBuffer.getMtContent().getItems().size()) {
                    mtContentBuffer.setCompleted(true);
                }

                if(skipped == set.size() && isCaptionReadComplete) {
                    throw bufferEmptyException;
                }
            }
        }

        return pagedMtContent;
    }

    public PagingSupportedSessionManager.MtContentBuffer buffer(UssdResponse.MtContent content) {
        PagingSupportedSessionManager.MtContentBuffer mtContentBuffer = new PagingSupportedSessionManager.MtContentBuffer();
        mtContentBuffer.setMtContent(content);
        mtContentBuffer.setCaptionReadIndex(0);
        mtContentBuffer.setContentItemReadCount(0);

        return mtContentBuffer;
    }
}
