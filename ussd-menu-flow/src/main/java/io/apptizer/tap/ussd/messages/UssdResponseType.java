package io.apptizer.tap.ussd.messages;

/**
 * Created by dinesh on 7/26/17.
 */
public enum UssdResponseType {
    STATIC,
    FORM
}
