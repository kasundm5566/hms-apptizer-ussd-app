package io.apptizer.tap.ussd.facades;

import com.google.common.collect.ImmutableMap;
import io.apptizer.tap.ussd.client.TapUssdClient;
import io.apptizer.tap.ussd.config.TapApiConfigurations;
import io.apptizer.tap.ussd.core.menus.templates.InvalidInputMenuTemplate;
import io.apptizer.tap.ussd.domain.UssdMoRequest;
import io.apptizer.tap.ussd.domain.UssdMoResponse;
import io.apptizer.tap.ussd.domain.UssdMtRequest;
import io.apptizer.tap.ussd.messages.UssdMoType;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.PagingSupportedSessionManager;
import io.apptizer.tap.ussd.util.SpecialCharacterFilterUtil;
import io.apptizer.tap.ussd.core.menus.util.UssdResponseFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Created by isuru on 3/15/2017.
 */
@Component
public class TapUssdFacade {

    private final TapUssdClient tapUssdClient;
    private final PagingSupportedSessionManager pagingSupportManager;
    private final TapApiConfigurations tapApiConfigurations;
    private final SpecialCharacterFilterUtil specialCharacterFilterUtil;
    private final UssdResponseFormatter ussdResponseFormatter;

    private final Map<String, UssdMoType> ussdMoMessageTypeMap = new ImmutableMap.Builder<String, UssdMoType>().
            put("mo-init", UssdMoType.MO_INIT).
            put("mo-cont", UssdMoType.MO_CONT).
            build();

    private final Map<UssdMtType, String> ussdMtTypeMap = new ImmutableMap.Builder<UssdMtType, String>().
            put(UssdMtType.MT_CONT, "mt-cont").
            put(UssdMtType.MT_FIN, "mt-fin").
            build();

    @Autowired
    public TapUssdFacade(TapUssdClient tapUssdClient,
                         PagingSupportedSessionManager pagingSupportManager,
                         TapApiConfigurations tapApiConfigurations,
                         SpecialCharacterFilterUtil specialCharacterFilterUtil,
                         UssdResponseFormatter ussdResponseFormatter) {
        this.tapUssdClient = tapUssdClient;
        this.pagingSupportManager = pagingSupportManager;
        this.tapApiConfigurations = tapApiConfigurations;
        this.specialCharacterFilterUtil = specialCharacterFilterUtil;
        this.ussdResponseFormatter = ussdResponseFormatter;
    }

    @Autowired
    private InvalidInputMenuTemplate invalidInputMenuTemplate;

    @PostConstruct
    public void init() {
        invalidInputMenuTemplate.setPagingSupportedSessionManager(pagingSupportManager);
    }

    public UssdMoResponse handleMo(UssdMoRequest request) {
        UssdResponse ussdResponse = pagingSupportManager.view(request.getApplicationId(),
                request.getSourceAddress(),
                request.getMessage(),
                ussdMoMessageTypeMap.get(request.getUssdOperation()), request.getSessionId());

        UssdMtRequest ussdMtRequest = new UssdMtRequest();
        ussdMtRequest.setApplicationId(tapApiConfigurations.getAppId());
        ussdMtRequest.setDestinationAddress(request.getSourceAddress());
        String message;
        if (ussdResponse.getResponseType().equals(UssdResponseType.STATIC)) {
            message = ussdResponseFormatter.formatUssdStaticResponse(specialCharacterFilterUtil.filterSpecialCharacters(ussdResponse));
        } else {
            message = ussdResponseFormatter.formatUssdFormResponse(specialCharacterFilterUtil.filterSpecialCharacters(ussdResponse));
        }

        ussdMtRequest.setMessage(message);
        ussdMtRequest.setUssdOperation(ussdMtTypeMap.get(ussdResponse.getMtType()));
        ussdMtRequest.setPassword(tapApiConfigurations.getPassword());
        ussdMtRequest.setSessionId(request.getSessionId());

        tapUssdClient.sendMt(ussdMtRequest);
        return new UssdMoResponse();
    }
}
