package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.session.Session;

import javax.annotation.PostConstruct;

/**
 * Created by dinesh on 3/27/17.
 */
abstract class AbstractMenuTemplate implements MenuTemplate {

    protected final String menuId;
    protected final MenuResolver menuResolver;

    @PostConstruct
    protected final void init() {
        menuResolver.register(menuId, this);
    }

    protected AbstractMenuTemplate(String menuId, MenuResolver menuResolver) {
        this.menuId = menuId;
        this.menuResolver = menuResolver;
    }

    public abstract MenuTemplate next(String input, Session session);

    public abstract UssdResponse content(Session session);
}
