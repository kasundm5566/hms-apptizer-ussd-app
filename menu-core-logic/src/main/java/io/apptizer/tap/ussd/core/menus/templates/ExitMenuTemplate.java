package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by dinesh on 3/20/17.
 * Exit menu of the application.
 */

@Component
@Qualifier("exitMenuTemplate")
@Primary
public class ExitMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public ExitMenuTemplate(MenuResolver menuResolver,
                            MessageReader messageReader,
                            UssdContentRenderer ussdContentRenderer) {
        super("exitMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        return null; // No next menu after the exit.
    }

    @Override
    public UssdResponse content(Session session) {
        // Menu content
        String message = messageReader.readMessage("exit.menu.message", new Object[]{});
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(message);
        mtContent.setItems(new HashMap<>());
        return new UssdResponse(mtContent, UssdMtType.MT_FIN, UssdResponseType.STATIC);
    }

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "ExitMenu";
    }
}
