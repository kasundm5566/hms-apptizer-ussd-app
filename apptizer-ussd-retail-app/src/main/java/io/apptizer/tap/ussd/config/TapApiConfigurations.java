package io.apptizer.tap.ussd.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by isuru on 3/15/2017.
 */
@Component
@ConfigurationProperties(prefix = "tap")
public class TapApiConfigurations {

    private String ussdMtUrl;
    private String appId;
    private String password;

    public String getUssdMtUrl() {
        return ussdMtUrl;
    }

    public String getAppId() {
        return appId;
    }

    public String getPassword() {
        return password;
    }

    public void setUssdMtUrl(String ussdMtUrl) {
        this.ussdMtUrl = ussdMtUrl;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
