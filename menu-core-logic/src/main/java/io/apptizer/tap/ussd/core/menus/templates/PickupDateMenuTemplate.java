package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.PickupService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/27/17.
 */

@Component
public class PickupDateMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final PickupService pickupService;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public PickupDateMenuTemplate(MenuResolver menuResolver,
                                  PickupService pickupService,
                                  MessageReader messageReader,
                                  UssdContentRenderer ussdContentRenderer) {
        super("pickupDateMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.pickupService = pickupService;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        String pickupDate = session.getContext().get(KeyBox.PICKUP_DATE + input);
        session.getContext().put(KeyBox.SELECTED_PICKUP_DATE, pickupDate);
        return menuResolver.getMenu("pickupTimeMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        List<String> upcomingWorkingDays = pickupService.getUpcomingWorkingDays();
        String caption = messageReader.readMessage("pickup.date", new Object[]{});
        Map<Integer, String> datesMap = new HashMap<>();
        for (int i = 0; i < upcomingWorkingDays.size(); i++) {
            datesMap.put(i + 1, upcomingWorkingDays.get(i));
            session.getContext().put(KeyBox.PICKUP_DATE + (i + 1), upcomingWorkingDays.get(i));
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(datesMap);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "PickupDateMenu";
    }
}
