package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dinesh on 3/27/17.
 */

@Component
public class ProceedCheckoutMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public ProceedCheckoutMenuTemplate(MenuResolver menuResolver,
                                       MessageReader messageReader,
                                       UssdContentRenderer ussdContentRenderer) {
        super("proceedCheckoutMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        switch (input) {
            case "1":
                return menuResolver.getMenu("pickupDateMenu");
            case "2":
                return menuResolver.getMenu("categoryMenu");
            case "3":
                return menuResolver.getMenu("myCartMenu");
            default:
                return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String selectedItem = session.getContext().get(KeyBox.SELECTED_PRODUCT);

        String menuCaption = messageReader.readMessage("item.added.to.cart", new Object[]{selectedItem});
        Map<Integer, String> menuOptions = new HashMap<>();
        menuOptions.put(1, messageReader.readMessage("checkout.option1", new Object[]{}));
        menuOptions.put(2, messageReader.readMessage("checkout.option2", new Object[]{}));
        menuOptions.put(3, messageReader.readMessage("checkout.option3", new Object[]{}));

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(menuCaption);
        mtContent.setItems(menuOptions);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "ProceedCheckoutMenu";
    }
}
