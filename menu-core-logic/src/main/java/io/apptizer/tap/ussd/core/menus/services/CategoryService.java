package io.apptizer.tap.ussd.core.menus.services;

import io.apptizer.api.model.product.Categories;
import io.apptizer.api.model.product.Category;
import io.apptizer.tap.ussd.core.menus.config.ApiConfigurations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by dinesh on 3/17/17.
 * Contains methods to access resources related to categories.
 */
@Component
public class CategoryService {

    private final ApiConfigurations apiConfigurations;
    private final RestTemplate restTemplate;

    @Autowired
    public CategoryService(ApiConfigurations apiConfigurations,
                           RestTemplateBuilder restTemplateBuilder) {
        this.apiConfigurations = apiConfigurations;
        restTemplate = restTemplateBuilder.build();
    }

    /**
     * Retrieve all the categories for a business.
     *
     * @param businessId Business identification no (APP Id).
     * @return List of category objects.
     */
    public List<Category> listCategories(String businessId) {
        // Resource access url
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getCategoriesUrl();
        Categories categories = restTemplate.getForObject(url, Categories.class);
        return categories.getCategories();
    }
}
