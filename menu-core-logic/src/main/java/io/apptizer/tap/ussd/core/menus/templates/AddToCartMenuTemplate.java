package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dinesh on 3/21/17.
 */
@Component
public class AddToCartMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public AddToCartMenuTemplate(MenuResolver menuResolver,
                                 MessageReader messageReader,
                                 UssdContentRenderer ussdContentRenderer) {
        super("addToCartMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        switch (input) {
            case "1":
                return menuResolver.getMenu("productDetailsMenu");
            case "2":
                return menuResolver.getMenu("variantsMenu");
            default:
                return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String caption = session.getContext().get(KeyBox.SELECTED_PRODUCT);
        String option1 = messageReader.readMessage("add.cart.options1", new Object[]{});
        String option2 = messageReader.readMessage("add.cart.options2", new Object[]{});
        Map<Integer, String> menuOptions = new HashMap<>();
        menuOptions.put(1, option1);
        menuOptions.put(2, option2);
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(menuOptions);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "AddToCartMenu";
    }
}
