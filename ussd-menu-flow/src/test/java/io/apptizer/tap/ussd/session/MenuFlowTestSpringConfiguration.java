package io.apptizer.tap.ussd.session;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by isuru on 3/15/2017.
 */
@Configuration
@ComponentScan(basePackages = {
        "io.apptizer.tap.ussd.menu",
        "io.apptizer.tap.ussd.session",
})
public class MenuFlowTestSpringConfiguration {

    /*private MenuTemplate index = indexMenu();

    private MenuTemplate indexMenu() {
        return new MenuTemplate() {
            MenuTemplate menuTemplate = categoryMenu();

            @Override
            public MenuTemplate next(String input, Session session) {
                return menuTemplate;
            }

            @Override
            public UssdResponse content(Session session) {
                return new UssdResponse("Hi " + session.getMobileNumber() + " welcome ", UssdMtType.MT_CONT);
            }

            @Override
            public boolean isBackSupported() {
                return false;
            }
        };
    }

    private MenuTemplate categoryMenu() {
        return new MenuTemplate() {
            MenuTemplate menuTemplate = finalMenu();

            @Override
            public MenuTemplate next(String input, Session session) {
                return menuTemplate;
            }

            @Override
            public UssdResponse content(Session session) {
                return new UssdResponse("Electronics items", UssdMtType.MT_CONT);
            }

            @Override
            public boolean isBackSupported() {
                return false;
            }
        };
    }

    private MenuTemplate finalMenu() {
        return new MenuTemplate() {
            @Override
            public MenuTemplate next(String input, Session session) {
                return null;
            }

            @Override
            public UssdResponse content(Session session) {
                return new UssdResponse("Bye", UssdMtType.MT_FIN);
            }

            @Override
            public boolean isBackSupported() {
                return false;
            }
        };
    }

    private MenuTemplate sessionExpiredMenu() {
        return new MenuTemplate() {
            @Override
            public MenuTemplate next(String input, Session session) {
                return null;
            }

            @Override
            public UssdResponse content(Session session) {
                return new UssdResponse("Session expired", UssdMtType.MT_FIN);
            }

            @Override
            public boolean isBackSupported() {
                return false;
            }
        };
    }

    private MenuTemplate invalidInputMenuTemplate() {
        return new MenuTemplate() {
            @Override
            public MenuTemplate next(String input, Session session) {
                return null;
            }

            @Override
            public UssdResponse content(Session session) {
                return new UssdResponse("Invalid input", UssdMtType.MT_FIN);
            }

            @Override
            public boolean isBackSupported() {
                return false;
            }
        };
    }

    @Bean
    public MenuTemplate index() {
        return index;
    }

    @Bean
    public SessionManagerConfigs sessionManagerConfigs() {
        return new SessionManagerConfigs("99", "999", 60,"#123*","APP_000",160);
    }

    @Bean
    public MenuManager menuManager() {
        return new MenuManager(index, sessionExpiredMenu(), finalMenu(), invalidInputMenuTemplate());
    }*/
}
