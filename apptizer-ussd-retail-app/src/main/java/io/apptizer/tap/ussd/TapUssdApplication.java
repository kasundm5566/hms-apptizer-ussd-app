package io.apptizer.tap.ussd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by sajith on 2/20/17.
 */
@SpringBootApplication
public class TapUssdApplication {

    private static final Logger logger = LogManager.getLogger(TapUssdApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(TapUssdApplication.class, args);
    }
}
