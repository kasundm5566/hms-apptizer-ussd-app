package io.apptizer.tap.ussd.core.menus.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.apptizer.api.model.purchase.*;
import io.apptizer.tap.ussd.core.menus.config.ApiConfigurations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by dinesh on 3/31/17.
 */
@Component
public class PurchaseService {

    private final ApiConfigurations apiConfigurations;
    private final RestTemplate restTemplate;

    @Autowired
    public PurchaseService(ApiConfigurations apiConfigurations,
                           RestTemplateBuilder restTemplateBuilder) {
        this.apiConfigurations = apiConfigurations;
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Add an item to the cart
     *
     * @param businessId Business identification no (APP Id)
     * @param mobileNo   Customer mobile no
     * @param cartItems  Serialized cart items
     * @param purchase   Purchase object containing the purchased item details
     * @return Serialized cart items (updated)
     * @throws IOException
     */
    public String addToCart(String businessId, String mobileNo, String cartItems, Purchase purchase) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        AddToShoppingCart addToShoppingCart = objectMapper.readValue(cartItems, AddToShoppingCart.class);
        addToShoppingCart.getPurchases().add(purchase);

        String url = apiConfigurations.getBaseUrlTap() + businessId + apiConfigurations.getCartUrl();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        mobileNo = mobileNo.replaceFirst("tel:", "");
        headers.add("X-Mobile-Number", mobileNo);
        HttpEntity<AddToShoppingCart> entity = new HttpEntity<>(addToShoppingCart, headers);
        restTemplate.exchange(url, HttpMethod.PUT, entity, ShoppingCart.class);

        return objectMapper.writeValueAsString(addToShoppingCart);
    }

    /**
     * Retrieve the cart
     *
     * @param businessId Business identification no (APP Id)
     * @param mobileNo   Customer mobile no
     * @return
     */
    public ShoppingCart getCartDetails(String businessId, String mobileNo) {
        String url = apiConfigurations.getBaseUrlTap() + businessId + apiConfigurations.getCartUrl();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        mobileNo = mobileNo.replaceFirst("tel:", "");
        headers.add("X-Mobile-Number", mobileNo);
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<ShoppingCart> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, ShoppingCart.class);
        return responseEntity.getBody();
    }

    /**
     * Perform the cart checkout
     *
     * @param businessId   Business identification no (APP Id)
     * @param mobileNo     Customer mobile no
     * @param checkoutCart Cart object contains the details related to the checkout process
     * @return Transaction id
     */
    public PurchaseHistoryEntry checkout(String businessId, String mobileNo, CheckoutCart checkoutCart) {
        String url = apiConfigurations.getBaseUrlTap() + businessId + apiConfigurations.getCartCheckoutUrl();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        mobileNo = mobileNo.replaceFirst("tel:", "");
        headers.add("X-Mobile-Number", mobileNo);

        HttpEntity<CheckoutCart> entity = new HttpEntity<>(checkoutCart, headers);
        ResponseEntity<PurchaseHistoryEntry> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, PurchaseHistoryEntry.class);
        return responseEntity.getBody();
    }
}
