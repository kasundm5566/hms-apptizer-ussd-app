package io.apptizer.tap.ussd.core.menus.handlers;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by dinesh on 4/28/17.
 */
@Component
@ControllerAdvice
public class MenuExceptionHandler {

    @ExceptionHandler
    public void handleExceptions(Exception ex) {
        ex.printStackTrace();
    }
}
