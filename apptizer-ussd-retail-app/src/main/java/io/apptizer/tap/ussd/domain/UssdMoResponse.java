package io.apptizer.tap.ussd.domain;

/**
 * Created by sajith on 2/20/17.
 */
public class UssdMoResponse {

    private String statusCode = "S1000";
    private String statusDetail = "Success";

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }
}
