package io.apptizer.tap.ussd.menu;

import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.session.Session;

/**
 * Created by isuru on 3/15/2017.
 */
public interface MenuTemplate {

    MenuTemplate next(String input, Session session);

    UssdResponse content(Session session);

    default boolean isBackSupported() {
        return true;
    }
}
