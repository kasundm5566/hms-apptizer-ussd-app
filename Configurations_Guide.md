# Apptizer USSD App Configurations
Configurations related to the above mentioned app are defined in the file named **application.yml**.
Description of some configuration values has metioned below.
1. ussdMtUrl: http://core.sdp:7000/ussd/send
URL to access the USSD service.
**core.sdp** - TAP API

2. serviceCode: "(\\\\*123#)((?<index>[0-9]{1,4})#)"
This defines the pattern of the service code. Here the *123# can be replaced by the relevant service code.

3. baseUrl: http://core.apptizer.io:9091/business/
This url is used to access the apptizer api.
**core.apptizer.io** - Apptizer API

4. url: jdbc:mysql://mysql.apptizer.io:3306/apptizer_ussd?useSSL=false
This is related to the database configurations. Following things should be completed to initialize the database connection successfully.
    - Create a new database named **apptizer_ussd**.
    - Create a new user named **user** with the password of **password** and grant all the privileges to the database created in the previous step.
    - Restore the database backup file named **apptizer.sql**.
    - **mysql.apptizer.io** - MySQL server

5. sessionTimeOut: 300
Session will get expired when the time difference between last operation and current time is equals to the given time in seconds.

6. defaultLocale: en_US
Language setting. fr_FR for French.