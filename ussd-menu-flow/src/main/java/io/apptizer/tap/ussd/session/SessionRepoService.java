package io.apptizer.tap.ussd.session;

import java.util.Map;
import java.util.Optional;

/**
 * Created by isuru on 3/15/2017.
 */
public interface SessionRepoService {

    Session create(String appId, String msisdn);

    void upsert(String sessionId, Session session);

    Optional<Session> lookup(String sessionId);

    void invalidate(String sessionId);

}
