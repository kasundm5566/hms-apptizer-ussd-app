package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.product.Product;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.ProductService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dinesh on 3/22/17.
 */
@Component
public class ProductDetailsMenuTemplate extends AbstractMenuTemplate {

    private final ProductService productService;
    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public ProductDetailsMenuTemplate(ProductService productService,
                                      MenuResolver menuResolver,
                                      MessageReader messageReader,
                                      UssdContentRenderer ussdContentRenderer) {
        super("productDetailsMenu", menuResolver);
        this.productService = productService;
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        if (input.equals("1")) {
            return menuResolver.getMenu("variantsMenu");
        }
        return menuResolver.getMenu("invalidInputMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        String productId = session.getContext().get(KeyBox.SELECTED_PRODUCT_ID);
        Product product = productService.getProductDetails(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), productId);
        String productDetailsMsg = messageReader.readMessage("product.details", new Object[]{});
        productDetailsMsg += messageReader.readMessage("product.details.content", new Object[]{product.getName(), Jsoup.parse(product.getDescription()).text()});
        Map<Integer, String> menuOptions = new HashMap<>();
        menuOptions.put(1, messageReader.readMessage("product.details.option", new Object[]{}));

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(productDetailsMsg);
        mtContent.setItems(menuOptions);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "ProductDetailsMenu";
    }
}
