package io.apptizer.tap.ussd.core.menus.services;

import io.apptizer.api.model.product.*;
import io.apptizer.tap.ussd.core.menus.config.ApiConfigurations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by dinesh on 3/20/17.
 * Contains methods to access resources related to products.
 */

@Component
public class ProductService {

    private final ApiConfigurations apiConfigurations;
    private final RestTemplate restTemplate;

    @Autowired
    public ProductService(ApiConfigurations apiConfigurations,
                          RestTemplateBuilder restTemplateBuilder) {
        this.apiConfigurations = apiConfigurations;
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Retrieve all the products of a category for a business
     *
     * @param businessId Business identification no (APP Id)
     * @param categoryId Category identification no to retrieve products
     * @return List of ProductSummary objects containing the products
     */
    public List<ProductSummary> listProducts(String businessId, String categoryId) {
        // Resource access url
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getCategoriesUrl() + categoryId + apiConfigurations.getProductsUrl();
        CategoryProductSummaries allProducts = restTemplate.getForObject(url, CategoryProductSummaries.class);
        return allProducts.getProductSummaries();
    }

    /**
     * Retrieve the details of a specific product
     *
     * @param businessId Business identification no (APP Id)
     * @param productId  Product identification no to retrieve product details
     * @return ProductSummary object containing the product details
     */
    public Product getProductDetails(String businessId, String productId) {
        // Resource access url
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getProductsUrl() + productId;
        Product product = restTemplate.getForObject(url, Product.class);
        return product;
    }

    /**
     * Retrieve the variant types of a specific product
     *
     * @param businessId Business identification no (APP Id)
     * @param productId  Product identification no to retrieve product variants
     * @return List of VariantType objects containing the variants of a product
     */
    public List<VariantType> getProductVariants(String businessId, String productId) {
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getProductsUrl() + productId;
        Product product = restTemplate.getForObject(url, Product.class);
        return product.getVariants().getTypes();
    }

    /**
     * Retrieve the add-ons of a specific product
     *
     * @param businessId Business identification no (APP Id)
     * @param productId  Product identification no to retrieve product variants
     * @return List of Addon objects containing the variants of a product
     */
    public List<AddOn> getProductAddOns(String businessId, String productId) {
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getProductsUrl() + productId;
        Product product = restTemplate.getForObject(url, Product.class);
        return product.getAddOns();
    }
}
