package io.apptizer.tap.ussd.core.menus.config;

/**
 * Created by dinesh on 4/4/17.
 */
public final class KeyBox {
    public static final String SELECTED_BUSINESS_ID = "selectedBusinessId";
    public static final String BUSINESS_ID = "businessId";
    public static final String BUSINESS_NAME = "businessName";
    public static final String SELECTED_BUSINESS_NAME = "selectedBusinessName";
    public static final String SELECTED_PRODUCT_ID = "selectedProductId";
    public static final String SELECTED_PRODUCT = "selectedProduct";
    public static final String ADDON_ITEM_NO = "addOnItemNo";
    public static final String ADDON_ID = "addOnId";
    public static final String ADDON_ID1 = "addOnId1";
    public static final String ADDON_NAME = "addOnName";
    public static final String ADDON_NAME1 = "addOnName1";
    public static final String ADDON_NAME2 = "addOnName2";
    public static final String CATEGORY_ID = "categoryId";
    public static final String CATEGORY_NAME = "categoryName";
    public static final String SELECTED_CATEGORY_ID = "selectedCategoryId";
    public static final String SELECTED_CATEGORY_NAME = "selectedCategoryName";
    public static final String PRODUCT_ID = "productId";
    public static final String PRODUCT_NAME = "productName";
    public static final String PICKUP_DATE = "pickupDate";
    public static final String SELECTED_PICKUP_DATE = "selectedPickupDate";
    public static final String PICKUP_TIME = "pickupTime";
    public static final String SELECTED_PICKUP_TIME = "selectedPickupTime";
    public static final String PICKUP_PERSON = "pickupPerson";
    public static final String VARIANT_ID = "variantId";
    public static final String SELECTED_VARIANT_ID = "selectedVariantId";
    public static final String SELECTED_ADDON_IDS = "selectedAddOnIds";
    public static final String CART_ITEMS = "cartItems";
    public static final String BUSINESS_OPTION="businessOption";
}
