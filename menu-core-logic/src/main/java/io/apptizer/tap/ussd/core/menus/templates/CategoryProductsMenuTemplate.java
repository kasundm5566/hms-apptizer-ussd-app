package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.product.ProductSummary;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.ProductService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/17/17.
 * Menu to render details of products of a category.
 */
@Component
public class CategoryProductsMenuTemplate extends AbstractMenuTemplate {

    private final ProductService productService;
    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public CategoryProductsMenuTemplate(ProductService productService,
                                        MenuResolver menuResolver,
                                        MessageReader messageReader,
                                        UssdContentRenderer ussdContentRenderer) {
        super("categoryProductsMenu", menuResolver);
        this.productService = productService;
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        if (!session.getContext().containsKey(KeyBox.PRODUCT_ID + input)) {
            return menuResolver.getMenu("invalidInputMenu");
        } else {
            session.getContext().put(KeyBox.SELECTED_PRODUCT, session.getContext().get(KeyBox.PRODUCT_NAME + input));
            session.getContext().put(KeyBox.SELECTED_PRODUCT_ID, session.getContext().get(KeyBox.PRODUCT_ID + input));
            return menuResolver.getMenu("addToCartMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String categoryId = session.getContext().get(KeyBox.SELECTED_CATEGORY_ID);
        // Menu content
        List<ProductSummary> productList = productService.listProducts(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), categoryId);
        String caption = session.getContext().get(KeyBox.SELECTED_CATEGORY_NAME);

        Map<Integer, String> productsMap = new HashMap<>();
        if (productList.size() > 0) {
            for (int i = 0; i < productList.size(); i++) {
                productsMap.put(i + 1, productList.get(i).getName());
                session.getContext().put(String.valueOf(KeyBox.PRODUCT_NAME + (i + 1)), productList.get(i).getName());
                session.getContext().put(String.valueOf(KeyBox.PRODUCT_ID + (i + 1)), productList.get(i).getProductId());
            }
        } else {
            caption += "<br/>" + messageReader.readMessage("categories.not.found.message", new Object[]{});
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(productsMap);

        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "CategoryProductsMenu";
    }
}
