package io.apptizer.tap.ussd.session;

import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMoType;
import io.apptizer.tap.ussd.messages.UssdResponse;

/**
 * Created by isuru on 3/15/2017.
 */
public interface SessionManager {
    UssdResponse view(String appId, String msisdn, String input, UssdMoType moType, String sessionId);

    MenuTemplate back(Session session);
}
