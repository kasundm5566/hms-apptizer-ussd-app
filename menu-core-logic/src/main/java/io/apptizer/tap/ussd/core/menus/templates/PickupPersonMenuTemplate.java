package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dinesh on 4/3/17.
 */

@Component
public class PickupPersonMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;


    @Autowired
    public PickupPersonMenuTemplate(MenuResolver menuResolver,
                                    MessageReader messageReader,
                                    UssdContentRenderer ussdContentRenderer) {
        super("pickupPersonMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        String regex = "[A-Za-z\\s]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            session.getContext().put(KeyBox.PICKUP_PERSON, input);
            return menuResolver.getMenu("checkoutConfirmMenu");
        } else {
            return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String caption = messageReader.readMessage("pickup.person", new Object[]{});
        String menuContent = ussdContentRenderer.renderFormMenu(caption);

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(new HashMap<>());
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.FORM);
    }

    public String toString() {
        return "PickupPersonMenu";
    }
}
