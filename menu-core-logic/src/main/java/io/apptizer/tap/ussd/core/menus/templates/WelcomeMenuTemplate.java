package io.apptizer.tap.ussd.core.menus.templates;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import io.apptizer.api.model.merchant.BusinessSummary;
import io.apptizer.api.model.purchase.AddToShoppingCart;
import io.apptizer.api.model.purchase.Purchase;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.BusinessService;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/17/17.
 */
@Component
@Qualifier("welcome")
@Primary
public class WelcomeMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final BusinessService businessService;
    private final UssdContentRenderer ussdContentRenderer;
    private final static Logger logger = LogManager.getLogger(WelcomeMenuTemplate.class);

    @Autowired
    public WelcomeMenuTemplate(MenuResolver menuResolver,
                               MessageReader messageReader,
                               BusinessService businessService,
                               UssdContentRenderer ussdContentRenderer) {
        super("welcomeMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.businessService = businessService;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        switch (input) {
            case "1":
                return menuResolver.getMenu("categoryMenu");
            case "2":
                return menuResolver.getMenu("myCartMenu");
            case "3":
                return menuResolver.getMenu("contactUsMenu");
            default:
                return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        if (session.getContext().get(KeyBox.SELECTED_BUSINESS_NAME) == null) {
            List<BusinessSummary> businessSummaries = businessService.getAllBusinessesTap();
            String requestedBusinessId = session.getContext().get(KeyBox.BUSINESS_OPTION); // e.g: APP_000050

            for (int i = 0; i < businessSummaries.size(); i++) {
                if (businessSummaries.get(i).getBusinessId().equals(requestedBusinessId)) {
                    session.getContext().put(KeyBox.SELECTED_BUSINESS_NAME, businessSummaries.get(i).getBusinessName());
                    session.getContext().put(KeyBox.SELECTED_BUSINESS_ID, requestedBusinessId);
                }
            }
        }
        if (businessService.checkBusinessStatus(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID)) == 403) {
            ThreadContext.put("destinationAddress", session.getMobileNumber());
            logger.error("Selected business " + session.getContext().get(KeyBox.SELECTED_BUSINESS_NAME) + " has expired.");
            String caption = messageReader.readMessage("business.expired.message", new Object[]{});

            UssdResponse.MtContent mtContent = new UssdResponse.MtContent();
            mtContent.setCaption(caption);
            mtContent.setItems(new HashMap<>());
            return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
        }

        if (Strings.isNullOrEmpty(session.getContext().get(KeyBox.SELECTED_BUSINESS_NAME))) {
            ThreadContext.put("destinationAddress", session.getMobileNumber());
            logger.error("No business found for the request.");
            String caption = messageReader.readMessage("business.not.found.message", new Object[]{});
            String businessNotFoundMessage = ussdContentRenderer.renderStaticMenu(caption, new HashMap<>());

            UssdResponse.MtContent mtContent = new UssdResponse.MtContent();
            mtContent.setCaption(caption);
            mtContent.setItems(new HashMap<>());
            return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
        }

        String menuContent = messageReader.readMessage("welcome.message.format", new Object[]{session.getContext().get(KeyBox.SELECTED_BUSINESS_NAME)});
        String menuOption1 = messageReader.readMessage("welcome.menu.option1", new Object[]{});
        String menuOption2 = messageReader.readMessage("welcome.menu.option2", new Object[]{});
        String menuOption3 = messageReader.readMessage("welcome.menu.option3", new Object[]{});
        Map<Integer, String> menuOptions = new HashMap<>();
        menuOptions.put(1, menuOption1);
        menuOptions.put(2, menuOption2);
        menuOptions.put(3, menuOption3);

        UssdResponse.MtContent mtContent = new UssdResponse.MtContent();
        mtContent.setCaption(menuContent);
        mtContent.setItems(menuOptions);
        if (!session.getContext().containsKey(KeyBox.CART_ITEMS)) {
            List<Purchase> purchaseList = new ArrayList<>();
            AddToShoppingCart addToShoppingCart = new AddToShoppingCart();
            addToShoppingCart.setPurchases(purchaseList);
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                session.getContext().put(KeyBox.CART_ITEMS, objectMapper.writeValueAsString(addToShoppingCart));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    /*@Override
    public UssdResponse content(Session session) {
        String message = "";
        if (session.getContext().get(KeyBox.SELECTED_BUSINESS_NAME) == null) {
            int businessOption = Integer.parseInt(session.getContext().get(KeyBox.BUSINESS_OPTION));
            Business business = businessService.getAllBusinesses().get(businessOption-1);
            session.getContext().put(KeyBox.SELECTED_BUSINESS_NAME, business.getName());
            session.getContext().put(KeyBox.SELECTED_BUSINESS_ID, business.getAppId());
        }

        message += messageReader.readMessage("welcome.message.format", new Object[]{session.getContext().get(KeyBox.SELECTED_BUSINESS_NAME)});
        message += messageReader.readMessage("back.exit.operation", new Object[]{appConfigurations.getBackButton(), appConfigurations.getExitButton()});
        if (!session.getContext().containsKey(KeyBox.CART_ITEMS)) {
            List<Purchase> purchaseList = new ArrayList<>();
            AddToShoppingCart addToShoppingCart = new AddToShoppingCart();
            addToShoppingCart.setPurchases(purchaseList);
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                session.getContext().put(KeyBox.CART_ITEMS, objectMapper.writeValueAsString(addToShoppingCart));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return new UssdResponse(message, UssdMtType.MT_CONT);
    }*/

    public String toString() {
        return "WelcomeMenu";
    }
}
