package io.apptizer.tap.ussd.interceptors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created by dinesh on 4/28/17.
 */
@Component
public class RestTemplateInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        ClientHttpResponse response = execution.execute(request, body);
        doLog(request, body, response);
        return response;
    }

    private void doLog(HttpRequest request, byte[] body, ClientHttpResponse response) throws IOException {
        logger.info("client request- {}|{}|{}", request.getURI(), request.getHeaders(), new String(body, Charset.forName("UTF-8")));
        logger.info("client response- {}|{}|{}", response.getStatusCode(), response.getHeaders(), new BufferedReader(new InputStreamReader(response.getBody())).readLine());
    }
}
