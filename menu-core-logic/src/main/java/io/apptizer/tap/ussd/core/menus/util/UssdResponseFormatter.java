package io.apptizer.tap.ussd.core.menus.util;

import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.session.SessionManagerConfigs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 7/26/17.
 */
@Component
public class UssdResponseFormatter {

    private final SessionManagerConfigs sessionManagerConfigs;
    private final MessageReader messageReader;

    @Autowired
    public UssdResponseFormatter(SessionManagerConfigs sessionManagerConfigs,
                                 MessageReader messageReader) {
        this.sessionManagerConfigs = sessionManagerConfigs;
        this.messageReader = messageReader;
    }

    // Format static menu content.
    public String formatUssdStaticResponse(UssdResponse response) {
        UssdResponse.MtContent mtContent = response.getContent();
        String caption = mtContent.getCaption();
        Map<Integer, String> items = mtContent.getItems();
        String formattedMsg = "<body>" + caption + "<br/>";

        List<Integer> itemsKeyset = new ArrayList<>(items.keySet());

        for (int i = 0; i < items.size(); i++) {
            String item = items.get(itemsKeyset.get(i));
            formattedMsg += itemsKeyset.get(i) + ". " + item + "<br/>";
        }

        if (response.isHasMorePage()) {
            formattedMsg += sessionManagerConfigs.getPaggingButton() + ". " + messageReader.readMessage("next.operation", new Object[]{}) + "<br/>";
        }

        formattedMsg += "<form action=\"response.html\"><input type=\"text\" name=\"response\"/></form>";
        formattedMsg += "</body>";
        return formattedMsg;
    }

    // Format form menu content.
    public String formatUssdFormResponse(UssdResponse response) {
        UssdResponse.MtContent mtContent = response.getContent();
        String caption = mtContent.getCaption();
        String formattedMsg = "<body>" + caption + "<br/>";
        formattedMsg += "<form action=\"response.html\"><input type=\"text\" name=\"response\"/></form>";
        formattedMsg += "</body>";
        return formattedMsg;
    }
}
