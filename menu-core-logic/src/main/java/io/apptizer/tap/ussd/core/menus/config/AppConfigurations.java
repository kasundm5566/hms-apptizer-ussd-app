package io.apptizer.tap.ussd.core.menus.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by dinesh on 3/21/17.
 */

@Component
@ConfigurationProperties(prefix = "ussdApp")
public class AppConfigurations {
    private int sessionTimeOut;
    private String backButton;
    private String exitButton;
    private String paggingButton;
    private String defaultLocale;
    private String serviceCode;
    private String businessIdPrefix;
    private int msgChunkSize;

    public int getSessionTimeOut() {
        return sessionTimeOut;
    }

    public void setSessionTimeOut(int sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
    }

    public String getBackButton() {
        return backButton;
    }

    public void setBackButton(String backButton) {
        this.backButton = backButton;
    }

    public String getExitButton() {
        return exitButton;
    }

    public void setExitButton(String exitButton) {
        this.exitButton = exitButton;
    }

    public String getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getBusinessIdPrefix() {
        return businessIdPrefix;
    }

    public void setBusinessIdPrefix(String businessIdPrefix) {
        this.businessIdPrefix = businessIdPrefix;
    }

    public int getMsgChunkSize() {
        return msgChunkSize;
    }

    public void setMsgChunkSize(int msgChunkSize) {
        this.msgChunkSize = msgChunkSize;
    }

    public String getPaggingButton() {
        return paggingButton;
    }

    public void setPaggingButton(String paggingButton) {
        this.paggingButton = paggingButton;
    }
}
