package io.apptizer.tap.ussd.menu;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by isuru on 3/15/2017.
 */
public class MenuManager {

    private final MenuTemplate indexMenuTemplate;
    private final MenuTemplate sessionExpiredMenuTemplate;
    private final MenuTemplate exitMenuTemplate;
    private final MenuTemplate invalidInputMenuTemplate;

    @Autowired
    public MenuManager(MenuTemplate indexMenuTemplate,
                       MenuTemplate sessionExpiredMenuTemplate,
                       MenuTemplate exitMenuTemplate,
                       MenuTemplate invalidInputMenuTemplate) {
        this.indexMenuTemplate = indexMenuTemplate;
        this.sessionExpiredMenuTemplate = sessionExpiredMenuTemplate;
        this.exitMenuTemplate = exitMenuTemplate;
        this.invalidInputMenuTemplate = invalidInputMenuTemplate;
    }

    public MenuTemplate getIndexMenuTemplate() {
        return indexMenuTemplate;
    }

    public MenuTemplate getSessionExpiredMenuTemplate() {
        return sessionExpiredMenuTemplate;
    }

    public MenuTemplate getExitMenuTemplate() {
        return exitMenuTemplate;
    }

    public MenuTemplate getInvalidInputMenuTemplate() {
        return invalidInputMenuTemplate;
    }
}
