package io.apptizer.tap.ussd.core.menus;

import io.apptizer.tap.ussd.core.menus.config.AppConfigurations;
import io.apptizer.tap.ussd.menu.MenuManager;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.session.SessionManagerConfigs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author isuruw@hsenidmobile.com
 */
@Configuration
public class ApptizerUssdSpringConfiguration {

    @Autowired
    @Qualifier("index")
    MenuTemplate indexMenuTemplate;

    @Autowired
    @Qualifier("sessionExpiredMenuTemplate")
    MenuTemplate sessionExpiredMenuTemplate;

    @Autowired
    @Qualifier("exitMenuTemplate")
    MenuTemplate exitMenuTemplate;

    @Autowired
    @Qualifier("invalidInputMenuTemplate")
    MenuTemplate invalidInputMenuTemplate;

    @Autowired
    AppConfigurations appConfigurations;

    @Bean
    public SessionManagerConfigs sessionManagerConfigs() {
        return new SessionManagerConfigs(appConfigurations.getBackButton(), appConfigurations.getExitButton(), appConfigurations.getPaggingButton(), appConfigurations.getSessionTimeOut(), appConfigurations.getServiceCode(),appConfigurations.getBusinessIdPrefix(),appConfigurations.getMsgChunkSize());
    }

    @Bean
    public MenuManager menuManager() {
        return new MenuManager(indexMenuTemplate, sessionExpiredMenuTemplate, exitMenuTemplate, invalidInputMenuTemplate);
    }
}
