package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.PagingSupportedSessionManager;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by dinesh on 3/31/17.
 */

@Component
@Qualifier("invalidInputMenuTemplate")
@Primary
public class InvalidInputMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public InvalidInputMenuTemplate(MenuResolver menuResolver,
                                    MessageReader messageReader,
                                    UssdContentRenderer ussdContentRenderer) {
        super("invalidInputMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    private PagingSupportedSessionManager pagingSupportedSessionManager;

    public void setPagingSupportedSessionManager(PagingSupportedSessionManager pagingSupportedSessionManager) {
        this.pagingSupportedSessionManager = pagingSupportedSessionManager;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        return pagingSupportedSessionManager.back(session);
    }

    @Override
    public UssdResponse content(Session session) {
        String invalidInputMsg = messageReader.readMessage("invalid.input.message", new Object[]{});

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(invalidInputMsg);
        mtContent.setItems(new HashMap<>());
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "InvalidInputMenu";
    }
}
