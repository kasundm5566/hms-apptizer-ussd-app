package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dinesh on 4/3/17.
 */

@Component
public class CheckoutSuccessMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    protected CheckoutSuccessMenuTemplate(MenuResolver menuResolver,
                                          MessageReader messageReader,
                                          UssdContentRenderer ussdContentRenderer) {
        super("checkoutSuccessMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        switch (input) {
            case "1":
                return menuResolver.getMenu("welcomeMenu");
            default:
                return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String message = session.getContext().get("checkoutMsg");
        Map<Integer, String> optionsMap = new HashMap<>();
        optionsMap.put(1, messageReader.readMessage("checkout.success.main.menu", new Object[]{}));

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(message);
        mtContent.setItems(optionsMap);

        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "CheckoutSuccessMenu";
    }
}
