package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by dinesh on 3/21/17.
 * This menu will appear when the user send a mo-cont after exit.
 */

@Component
@Qualifier("sessionExpiredMenuTemplate")
@Primary
public class SessionExpiredMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public SessionExpiredMenuTemplate(MenuResolver menuResolver,
                                      MessageReader messageReader,
                                      UssdContentRenderer ussdContentRenderer) {
        super("sessionExpiredMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        return null; // No next menu after session expired.
    }

    @Override
    public UssdResponse content(Session session) {
        // Menu content
        String caption = messageReader.readMessage("session.expired.message", new Object[]{});

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(new HashMap<>());
        return new UssdResponse(mtContent, UssdMtType.MT_FIN, UssdResponseType.STATIC);
    }

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "SessionExpiredMenu";
    }
}
