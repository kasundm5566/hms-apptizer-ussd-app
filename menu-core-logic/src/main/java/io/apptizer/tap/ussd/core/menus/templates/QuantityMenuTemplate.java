package io.apptizer.tap.ussd.core.menus.templates;

import com.google.common.base.Strings;
import io.apptizer.api.model.purchase.Purchase;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.PurchaseService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dinesh on 3/24/17.
 */

@Component
public class QuantityMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final PurchaseService purchaseService;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public QuantityMenuTemplate(MenuResolver menuResolver,
                                PurchaseService purchaseService,
                                MessageReader messageReader,
                                UssdContentRenderer ussdContentRenderer) {
        super("quantityMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.purchaseService = purchaseService;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        if (StringUtils.isNumeric(input)) {
            String productId = session.getContext().get(KeyBox.SELECTED_PRODUCT_ID);
            String variantId = session.getContext().get(KeyBox.SELECTED_VARIANT_ID);
            String quantity = input;
            String addOns = session.getContext().get(KeyBox.SELECTED_ADDON_IDS);

            List<String> addOnsList = new ArrayList<>();
            if (!Strings.isNullOrEmpty(addOns)) {
                String addOnsArray[] = addOns.split(",");
                for (String addOn : addOnsArray) {
                    addOnsList.add(addOn);
                }
            }

            Purchase purchase = new Purchase();
            purchase.setProductId(productId);
            purchase.setVariantId(variantId);
            purchase.setQuantity(Integer.parseInt(quantity));
            purchase.setAddOnSubTypeIds(addOnsList);

            try {
                String updatedCartItems = purchaseService.addToCart(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), session.getMobileNumber(), session.getContext().get(KeyBox.CART_ITEMS), purchase);
                session.getContext().put(KeyBox.CART_ITEMS, updatedCartItems);
                session.getContext().remove(KeyBox.SELECTED_ADDON_IDS);
                session.getContext().remove(KeyBox.SELECTED_PRODUCT_ID);
                session.getContext().remove(KeyBox.SELECTED_VARIANT_ID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return menuResolver.getMenu("proceedCheckoutMenu");
        } else {
            return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        String caption = messageReader.readMessage("enter.quantity", new Object[]{});

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(new HashMap<>());
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.FORM);
    }

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "QuantityMenu";
    }
}
