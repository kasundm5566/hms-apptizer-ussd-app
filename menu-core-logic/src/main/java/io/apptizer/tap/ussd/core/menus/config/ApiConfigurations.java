package io.apptizer.tap.ussd.core.menus.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by dinesh on 3/20/17.
 */
@Configuration
@ConfigurationProperties(prefix = "api")
public class ApiConfigurations {
    private String baseUrl;
    private String categoriesUrl;
    private String productsUrl;
    private String businessDetailsUrl;
    private String cartUrl;
    private String baseUrlTap;
    private String cartCheckoutUrl;
    private String allBusinessDetailsUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getCategoriesUrl() {
        return categoriesUrl;
    }

    public void setCategoriesUrl(String categoriesUrl) {
        this.categoriesUrl = categoriesUrl;
    }

    public String getProductsUrl() {
        return productsUrl;
    }

    public void setProductsUrl(String productsUrl) {
        this.productsUrl = productsUrl;
    }

    public String getBusinessDetailsUrl() {
        return businessDetailsUrl;
    }

    public void setBusinessDetailsUrl(String businessDetailsUrl) {
        this.businessDetailsUrl = businessDetailsUrl;
    }

    public String getCartUrl() {
        return cartUrl;
    }

    public void setCartUrl(String cartUrl) {
        this.cartUrl = cartUrl;
    }

    public String getBaseUrlTap() {
        return baseUrlTap;
    }

    public void setBaseUrlTap(String baseUrlTap) {
        this.baseUrlTap = baseUrlTap;
    }

    public String getCartCheckoutUrl() {
        return cartCheckoutUrl;
    }

    public void setCartCheckoutUrl(String cartCheckoutUrl) {
        this.cartCheckoutUrl = cartCheckoutUrl;
    }

    public String getAllBusinessDetailsUrl() {
        return allBusinessDetailsUrl;
    }

    public void setAllBusinessDetailsUrl(String allBusinessDetailsUrl) {
        this.allBusinessDetailsUrl = allBusinessDetailsUrl;
    }
}
