package io.apptizer.tap.ussd.util;

import io.apptizer.tap.ussd.messages.UssdResponse;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 7/26/17.
 */
@Component
public class SpecialCharacterFilterUtil {

    public UssdResponse filterSpecialCharacters(UssdResponse response) {
        UssdResponse.MtContent content = response.getContent();
        String caption = content.getCaption();
        Map<Integer, String> items = content.getItems();

        // Loading the YAML file (special_characters.yaml in resources).
        Yaml yaml = new Yaml();
        InputStream inputStream = SpecialCharacterFilterUtil.class.getResourceAsStream("/special_characters.yaml");
        Map<String, String> charactersMap = (Map<String, String>) yaml
                .load(inputStream);
        // Key values of the special_characters.yaml (French characters).
        List<String> characterKeyset = new ArrayList<>(charactersMap.keySet());

        // Key values of the menu content.
        List<Integer> itemsKeyset = new ArrayList<>(items.keySet());

        // Replace special characters in message caption
        for (int i = 0; i < characterKeyset.size(); i++) {
            String key = characterKeyset.get(i);
            caption = caption.replaceAll(key, charactersMap.get(key));

            // Replace special characters in message content
            for (int j = 0; j < itemsKeyset.size(); j++) {
                String item = items.get(itemsKeyset.get(j));
                items.replace(itemsKeyset.get(j), item.replaceAll(key, charactersMap.get(key)));
            }
        }

        response.getContent().setCaption(caption);
        response.getContent().setItems(items);
        return response;
    }
}
