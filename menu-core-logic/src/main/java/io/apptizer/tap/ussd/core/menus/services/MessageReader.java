package io.apptizer.tap.ussd.core.menus.services;

import io.apptizer.tap.ussd.core.menus.config.AppConfigurations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by dinesh on 4/11/17.
 */
@Component
public class MessageReader {
    private final MessageSource messageSource;
    private final AppConfigurations appConfigurations;
    private final Locale locale;
    private Locale.Builder builder = new Locale.Builder();

    @Autowired
    public MessageReader(MessageSource messageSource,
                         AppConfigurations appConfigurations) {
        this.messageSource = messageSource;
        this.appConfigurations = appConfigurations;

        if (this.appConfigurations.getDefaultLocale().equals("") || this.appConfigurations.getDefaultLocale() == null) {
            this.locale = Locale.getDefault();
        } else {
            String[] defaultLocale = this.appConfigurations.getDefaultLocale().split("_");
            this.locale = builder.setLanguage(defaultLocale[0]).setRegion(defaultLocale[1]).build();
        }
    }

    /**
     * Read a message from a language property file.
     *
     * @param key        Message property key.
     * @param parameters Data to be included in the message.
     * @return Message for the given key.
     */
    public String readMessage(String key, Object[] parameters) {
        return messageSource.getMessage(key, parameters, locale);
    }
}
