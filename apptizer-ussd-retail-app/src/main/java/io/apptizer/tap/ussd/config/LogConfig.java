package io.apptizer.tap.ussd.config;

import io.apptizer.tap.ussd.filters.ServerLogFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by dinesh on 4/28/17.
 */
@Configuration
public class LogConfig {

    private final ServerLogFilter serverLogFilter;

    @Autowired
    public LogConfig(ServerLogFilter serverLogFilter) {
        this.serverLogFilter = serverLogFilter;
    }

    @Bean
    public FilterRegistrationBean requestDumperFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(serverLogFilter);
        registration.addUrlPatterns("/*");
        return registration;
    }
}
