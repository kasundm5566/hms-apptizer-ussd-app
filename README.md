# Apptizer USSD App

### Pre-requisites
Make sure you have installed the following software before proceed with the installation.
- Java Development Kit 1.8
- Maven 3.3.9
- NBL Application with USSD (Refer to TAP_SETUP.MD)

### DNS Setup
Add following DN entries to your host file
- Apptizer api service domain

    **`172.16.3.184 core.apptizer.io`**

- TAP NBL domain

    **`127.0.0.1 core.sdp`**

### Installation guide

### Starting the service
1. Extract the zip file in the target directory.
    **`ussd-retail-app/apptizer-ussd-retail-app/target/apptizer-ussd-retail-app-bin.zip`**
3. Browse to the bin directory of the extracted directory.
2. Start the application using the following command.
**`./apptizer-ussd-retail-app start`**


### Log Formatting

#### Tap Request Logging
Log Format
[Thread-Id][msg]

Message Format
request - [<request-body>]
response - [<response-body>]

#### Apptizer Client Api Calls logs
request - <path>|<headers>|<body>
response - <status>|<headers>|<body>