package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.purchase.*;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.PurchaseService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 4/18/17.
 */

@Component
public class CheckoutConfirmMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final PurchaseService purchaseService;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    protected CheckoutConfirmMenuTemplate(MenuResolver menuResolver,
                                          PurchaseService purchaseService,
                                          MessageReader messageReader,
                                          UssdContentRenderer ussdContentRenderer) {
        super("checkoutConfirmMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.purchaseService = purchaseService;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        switch (input) {
            case "1":
                String pickupDate = session.getContext().get(KeyBox.SELECTED_PICKUP_DATE);
                String pickupTime = session.getContext().get(KeyBox.SELECTED_PICKUP_TIME);
                String pickupPerson = session.getContext().get(KeyBox.PICKUP_PERSON);
                String collectionMethod = "PICK_UP";
                String paymentMethod = "CASH_ON_COLLECT";

                CheckoutCart checkoutCart = new CheckoutCart();
                CollectionInfo collectionInfo = new CollectionInfo();
                collectionInfo.setCollectorName(pickupPerson);
                collectionInfo.setRequestedCollectionTime(pickupDate + " " + pickupTime);
                checkoutCart.setCollectionInfo(collectionInfo);
                checkoutCart.setCollectionMethod(collectionMethod);
                checkoutCart.setPaymentMethod(paymentMethod);

                PurchaseHistoryEntry purchaseHistoryEntry = purchaseService.checkout(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), session.getMobileNumber(), checkoutCart);
                String transactionId = purchaseHistoryEntry.getTransactionId();
                String message = messageReader.readMessage("order.success.message", new Object[]{});
                message += messageReader.readMessage("order.no", new Object[]{transactionId}) + "<br/>";
                session.getContext().put("checkoutMsg", message);
                return menuResolver.getMenu("checkoutSuccessMenu");
            default:
                return menuResolver.getMenu("invalidInputMenu");
        }
    }

    @Override
    public UssdResponse content(Session session) {
        ShoppingCart shoppingCart = purchaseService.getCartDetails(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), session.getMobileNumber());
        String message = "";
        Map<Integer, String> optionsMap = new HashMap<>();
        if (shoppingCart != null) {
            message += messageReader.readMessage("checkout.items.title", new Object[]{});
            List<PurchaseEntry> purchaseEntries = shoppingCart.getPurchases();
            for (int i = 0; i < purchaseEntries.size(); i++) {
                Float itemPrice = purchaseEntries.get(i).getItemPrice();
                int qty = purchaseEntries.get(i).getCount();
                float total = itemPrice * qty;
                message += purchaseEntries.get(i).getProductName() + " " + String.format("%.2f", total) + "<br/>";
            }
            optionsMap.put(1, messageReader.readMessage("checkout.confirm.option", new Object[]{}));
        } else {
            message += messageReader.readMessage("empty.cart", new Object[]{});
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(message);
        mtContent.setItems(optionsMap);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "CheckoutConfirmMenu";
    }
}
