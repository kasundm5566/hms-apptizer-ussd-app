package io.apptizer.tap.ussd.core.menus.services;

import io.apptizer.api.model.merchant.BusinessOperationalHours;
import io.apptizer.api.model.merchant.BusinessSummary;
import io.apptizer.tap.ussd.core.menus.config.ApiConfigurations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dinesh on 3/28/17.
 */

@Component
public class PickupService {
    private final ApiConfigurations apiConfigurations;
    private final RestTemplate restTemplate;

    @Autowired
    public PickupService(ApiConfigurations apiConfigurations,
                         RestTemplateBuilder restTemplateBuilder) {
        this.apiConfigurations = apiConfigurations;
        this.restTemplate = restTemplateBuilder.build();
    }

    private enum Days {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        EVERYDAY
    }

    /**
     * Get the available dates for pickup.
     *
     * @return List of possible dates in string format.
     */
    public List<String> getUpcomingWorkingDays() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM");
        List<String> workingDays = new ArrayList<>();
        int requiredNoOfDays = 4;

        if (isWorkingDay(calendar)) {
            workingDays.add("Today");
            requiredNoOfDays = 3;
        }
        for (int i = 0; i < requiredNoOfDays; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            if (!isWorkingDay(calendar)) {
                i--;
                continue;
            }
            workingDays.add(dateFormat.format(calendar.getTime()));
        }

        return workingDays;
    }

    /**
     * Get the possible times for pickup according to the day.
     *
     * @param date       Date when the pickup is processing, eg: 01 March.
     * @param businessId Business identification no (APP Id).
     * @return List of String data containing the possible time.
     * @throws ParseException Exception when it is impossible to parse the date.
     */
    public List<String> getPickupTimes(String businessId, String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM");
        Date fromTime;
        Date toTime;
        Date currentTime;
        Calendar fromCalendar;
        Calendar toCalendar;
        Calendar currentCalendar;
        List<String> possiblePickupTimes = new ArrayList<>();
        boolean isTodaySelected = false;

        if (date.equals("Today")) {
            date = dateFormat.format(new Date());
            isTodaySelected = true;
        }

        dateFormat.applyPattern("HH:mm");
        Days day = getDay(date);
        Map<String, String> hashMap;

        hashMap = getBusinessHours(businessId, day);

        if (hashMap.size() > 0) {
            String openingFrom = hashMap.get("from");
            String openingTo = hashMap.get("to");

            fromTime = dateFormat.parse(openingFrom);
            toTime = dateFormat.parse(openingTo);
            currentTime = dateFormat.parse(dateFormat.format(new Date()));

            fromCalendar = Calendar.getInstance();
            fromCalendar.setTime(fromTime);
            toCalendar = Calendar.getInstance();
            toCalendar.setTime(toTime);
            currentCalendar = Calendar.getInstance();
            currentCalendar.setTime(currentTime);

            if (isTodaySelected && currentCalendar.compareTo(fromCalendar) > 0) {
                fromCalendar = currentCalendar;
            }
            while (fromCalendar.compareTo(toCalendar) != 0) {
                int unroundedMinutes = fromCalendar.get(Calendar.MINUTE);
                int mod = unroundedMinutes % 30;
                fromCalendar.add(Calendar.MINUTE, (30 - mod));
                possiblePickupTimes.add(dateFormat.format(fromCalendar.getTime()));
            }
        } else {
            possiblePickupTimes = getAllTimesOfTheDay(isTodaySelected);
        }

        return possiblePickupTimes;
    }

    /**
     * Get the day of a given date.
     *
     * @param date String date, eg: 01 March.
     * @return Day of the date.
     * @throws ParseException Exception when it is impossible to parse the date.
     */
    private Days getDay(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM y");
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        String yearInString = String.valueOf(year);
        date += " " + yearInString;
        Date convertedDate = dateFormat.parse(date);
        calendar.setTime(convertedDate);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case 1:
                return Days.SUNDAY;
            case 2:
                return Days.MONDAY;
            case 3:
                return Days.TUESDAY;
            case 4:
                return Days.WEDNESDAY;
            case 5:
                return Days.THURSDAY;
            case 6:
                return Days.FRIDAY;
            case 7:
                return Days.SATURDAY;
            default:
                return Days.EVERYDAY;
        }
    }

    /**
     * Check whether the given date by a calendar object is a working day.
     * According to the method, all the days except Sunday are working days.
     *
     * @param calendar Calendar object.
     * @return True if the day is a working day.
     */
    private boolean isWorkingDay(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        /*if (dayOfWeek == Calendar.SUNDAY) {
            return false;
        }*/
        return true;
    }

    /**
     * Get the business hours of a day.
     *
     * @param businessId Business identification no (APP Id).
     * @param day        Day type, eg: Day.MONDAY.
     * @return Map containing the opening and closing times.
     */
    private Map<String, String> getBusinessHours(String businessId, Days day) {
        String url = apiConfigurations.getBaseUrl() + businessId + apiConfigurations.getBusinessDetailsUrl();
        BusinessSummary businessSummary = restTemplate.getForObject(url, BusinessSummary.class);
        List<BusinessOperationalHours> operationalHoursList = businessSummary.getDeliveryHours();

        Map<String, String> businessHours = new HashMap<>();

        for (int i = 0; i < operationalHoursList.size(); i++) {
            if (operationalHoursList.get(i).getDayOfWeek().name().equals(day.name())) {
                businessHours.put("from", operationalHoursList.get(i).getFrom());
                businessHours.put("to", operationalHoursList.get(i).getTo());
            }
        }
        return businessHours;
    }

    private List<String> getAllTimesOfTheDay(boolean isToday) {
        DateFormat df = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        List<String> timeOfTheDay = new ArrayList<>();
        if (!isToday) {
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
        }else{
            int unroundedMinutes = calendar.get(Calendar.MINUTE);
            int mod = unroundedMinutes % 30;
            calendar.add(Calendar.MINUTE, (30 - mod));
        }

        int startDate = calendar.get(Calendar.DATE);
        while (calendar.get(Calendar.DATE) == startDate) {
            timeOfTheDay.add(df.format(calendar.getTime()));
            calendar.add(Calendar.MINUTE, 30);
        }
        return timeOfTheDay;
    }
}
