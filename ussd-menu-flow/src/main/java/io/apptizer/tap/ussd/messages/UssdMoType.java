package io.apptizer.tap.ussd.messages;

/**
 * Created by isuru on 3/15/2017.
 */
public enum UssdMoType {
    MO_CONT,
    MO_INIT
}
