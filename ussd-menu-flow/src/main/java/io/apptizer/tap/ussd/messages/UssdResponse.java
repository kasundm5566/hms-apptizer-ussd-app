package io.apptizer.tap.ussd.messages;

import java.util.Map;

/**
 * Created by isuru on 3/15/2017.
 */
public class UssdResponse {

    /**
     * Content {
     *     caption:
     *     key_set = [1-> val1, 2 -> val2]
     * }
     * */
    public static class MtContent {
        private String caption;
        private Map<Integer, String> items;

        public String getCaption() {
            return caption;
        }

        public void setCaption(String caption) {
            this.caption = caption;
        }

        public Map<Integer, String> getItems() {
            return items;
        }

        public void setItems(Map<Integer, String> items) {
            this.items = items;
        }
    }

    private final MtContent content;
    private final UssdMtType mtType;
    private final UssdResponseType responseType;
    private boolean hasMorePage;

    public UssdResponse(MtContent content,
                        UssdMtType mtType,
                        UssdResponseType responseType) {
        this.content = content;
        this.mtType = mtType;
        this.responseType = responseType;
    }

    public UssdResponse(MtContent content, UssdMtType mtType, UssdResponseType responseType, boolean hasMorePage) {
        this.content = content;
        this.mtType = mtType;
        this.responseType = responseType;
        this.hasMorePage = hasMorePage;
    }

    public MtContent getContent() {
        return content;
    }

    public UssdMtType getMtType() {
        return mtType;
    }

    public UssdResponseType getResponseType() {
        return responseType;
    }

    public boolean isHasMorePage() {
        return hasMorePage;
    }

    public void setHasMorePage(boolean hasMorePage) {
        this.hasMorePage = hasMorePage;
    }
}
