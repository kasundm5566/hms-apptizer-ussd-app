package io.apptizer.tap.ussd.session;

import io.apptizer.tap.ussd.messages.UssdMoType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by isuru on 3/15/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MenuFlowTestSpringConfiguration.class})
public class SessionManagerTest {


    @Autowired
    SessionManager sessionManager;

    @Test
    public void view() throws Exception {
        {
            UssdResponse ussdResponse = sessionManager.view("APP_00101", "077898828", "1", UssdMoType.MO_INIT,"123");
            Assert.assertNotNull(ussdResponse);
        }
        {
            UssdResponse ussdResponse = sessionManager.view("APP_00101", "077898828", "1", UssdMoType.MO_CONT,"123");
            Assert.assertNotNull(ussdResponse);
        }
        {
            UssdResponse ussdResponse = sessionManager.view("APP_00101", "077898828", "99", UssdMoType.MO_CONT,"123");
            Assert.assertNotNull(ussdResponse);
        }
        {
            UssdResponse ussdResponse = sessionManager.view("APP_00101", "077898828", "999", UssdMoType.MO_CONT,"123");
            Assert.assertNotNull(ussdResponse);
        }
    }

}