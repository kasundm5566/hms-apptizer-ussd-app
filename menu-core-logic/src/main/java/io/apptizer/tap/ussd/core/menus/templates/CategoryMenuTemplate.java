package io.apptizer.tap.ussd.core.menus.templates;

import com.google.common.base.Strings;
import io.apptizer.api.model.product.Category;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.CategoryService;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/21/17.
 */

@Component
public class CategoryMenuTemplate extends AbstractMenuTemplate {

    private final CategoryService categoryService;
    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public CategoryMenuTemplate(CategoryService categoryService,
                                MenuResolver menuResolver,
                                MessageReader messageReader,
                                UssdContentRenderer ussdContentRenderer) {
        super("categoryMenu", menuResolver);
        this.categoryService = categoryService;
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        String categoryId = session.getContext().get(KeyBox.CATEGORY_ID + input);
        if (Strings.isNullOrEmpty(categoryId)) {
            return menuResolver.getMenu("invalidInputMenu");
        }
        String categoryName = session.getContext().get(KeyBox.CATEGORY_NAME + input);
        session.getContext().put(KeyBox.SELECTED_CATEGORY_ID, categoryId);
        session.getContext().put(KeyBox.SELECTED_CATEGORY_NAME, categoryName);
        return menuResolver.getMenu("categoryProductsMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        // Menu content
        List<Category> categoryList = categoryService.listCategories(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID));
        Map<Integer, String> categoriesMap = new HashMap<>();

        String caption = messageReader.readMessage("categories.list", new Object[]{});
        String message;
        if (categoryList.size() > 0) {
            for (int i = 0; i < categoryList.size(); i++) {
                categoriesMap.put(i + 1, categoryList.get(i).getName());
                session.getContext().put(KeyBox.CATEGORY_ID + String.valueOf((i + 1)), categoryList.get(i).getCategoryId());
                session.getContext().put(KeyBox.CATEGORY_NAME + String.valueOf((i + 1)), categoryList.get(i).getName());
            }
        } else {
            caption = messageReader.readMessage("categories.not.found.message", new Object[]{});
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(categoriesMap);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "CategoryMenu";
    }
}
