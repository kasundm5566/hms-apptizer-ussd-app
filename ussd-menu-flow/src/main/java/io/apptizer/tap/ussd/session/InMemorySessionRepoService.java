package io.apptizer.tap.ussd.session;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Optional;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

/**
 * Created by isuru on 3/15/2017.
 */
@Component
@Qualifier("inMemorySessionRepoService")
public class InMemorySessionRepoService implements SessionRepoService {

    private final Cache<String, Session> cache;
    private final MenuTemplate index;
    private final SessionManagerConfigs sessionManagerConfigs;

    public InMemorySessionRepoService(@Qualifier("index") MenuTemplate index,
                                      SessionManagerConfigs sessionManagerConfigs) {
        this.sessionManagerConfigs = sessionManagerConfigs;
        this.cache = CacheBuilder.
                newBuilder().
                expireAfterAccess(sessionManagerConfigs.getSessionExpiryInSeconds(), TimeUnit.SECONDS).
                build(); {
        };
        this.index = index;
    }

    @Override
    public void upsert(String sessionId, Session session) {
        cache.put(sessionId, session);
    }

    public Session create(String appId, String msisdn) {
        Session session = new Session(appId, msisdn);
        session.setContext(new HashMap<>());
        Stack<MenuTemplate> menuTemplateSequence = new Stack<MenuTemplate>();
        menuTemplateSequence.push(index);
        session.setMenuTemplateSequence(menuTemplateSequence);
        cache.put(msisdn, session);
        return session;
    }

    public Optional<Session> lookup(String sessionId) {
        return Optional.ofNullable(cache.getIfPresent(sessionId));
    }

    public void invalidate(String sessionId) {
        cache.invalidate(sessionId);
    }
}
