package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.PickupService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/27/17.
 */

@Component
public class PickupTimeMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final PickupService pickupService;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public PickupTimeMenuTemplate(MenuResolver menuResolver,
                                  PickupService pickupService,
                                  MessageReader messageReader,
                                  UssdContentRenderer ussdContentRenderer) {
        super("pickupTimeMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.pickupService = pickupService;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        String pickupTime = session.getContext().get(KeyBox.PICKUP_TIME + input);
        session.getContext().put(KeyBox.SELECTED_PICKUP_TIME, pickupTime);
        return menuResolver.getMenu("pickupPersonMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        String caption = messageReader.readMessage("pickup.time", new Object[]{});
        Map<Integer, String> timesMap = new HashMap<>();
        try {
            List<String> pickupTimesList = pickupService.getPickupTimes(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), session.getContext().get(KeyBox.SELECTED_PICKUP_DATE));
            for (int i = 0; i < pickupTimesList.size(); i++) {
                timesMap.put(i + 1, pickupTimesList.get(i));
                session.getContext().put(KeyBox.PICKUP_TIME + (i + 1), pickupTimesList.get(i));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(timesMap);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "PickupTimeMenu";
    }
}
