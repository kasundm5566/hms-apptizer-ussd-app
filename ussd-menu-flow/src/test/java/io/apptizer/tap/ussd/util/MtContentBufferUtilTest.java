package io.apptizer.tap.ussd.util;

import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.session.PagingSupportedSessionManager;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

/**
 * Created by isuru on 7/26/2017.
 */
public class MtContentBufferUtilTest {
    @Test
    public void content() throws Exception {

        UssdResponse.MtContent mtContent = new UssdResponse.MtContent();
        mtContent.setCaption("1. Shutdown the apptizer-api-service\n" +
                "2. Go to /apptizer/apps/apptizer-api-service\n" +
                "3. Execute \n" +
                "\trm lib/tap-apptizer-plugin-0.1.*.jar\n" +
                "4. \tCopy the following files in the patch directory of this release to respective locaiton\n" +
                "    tap-apptizer-plugin-0.1.67.jar -> /apptizer/apps/apptizer-api-service/lib\n" +
                "\twrapper.conf -> /apptizer/apps/apptizer-api-service/conf\n" +
                "5. Restart apptizer-api-service\t");
        HashMap<Integer, String> items = new HashMap<>();
        items.put(1, "Copy the following files in the patch directory of this release to respective");
        items.put(2, "Copy the following files in the patch directory of this release to respective");
        items.put(3, "Copy the following files in the patch directory of this release to respective");
        items.put(4, "Copy the following files in the patch directory of this release to respective");
        items.put(5, "Copy the following files in the patch directory of this release to respective");
        items.put(6, "Copy the following files in the patch directory of this release to respective");
        items.put(7, "Copy the following files in the patch directory of this release to respective");
        items.put(8, "Copy the following files in the patch directory of this release to respective");
        mtContent.setItems(items);

        MtContentBufferUtil mtContentBufferUtil = new MtContentBufferUtil(160);
        PagingSupportedSessionManager.MtContentBuffer buffer = mtContentBufferUtil.buffer(mtContent);

        while (true) {
            UssdResponse.MtContent content = mtContentBufferUtil.content(buffer);
            Assert.assertNotNull(content);
        }
    }

    @Test
    public void buffer() throws Exception {

    }

}