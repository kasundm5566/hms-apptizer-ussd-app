package io.apptizer.tap.ussd.domain;

/**
 * Created by sajith on 2/20/17.
 */
public class UssdMoRequest {
    private String message;
    private String ussdOperation;
    private String requestId;
    private String vlrAddress;
    private String sessionId;
    private String encoding;
    private String sourceAddress;
    private String applicationId;
    private String version;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUssdOperation() {
        return ussdOperation;
    }

    public void setUssdOperation(String ussdOperation) {
        this.ussdOperation = ussdOperation;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getVlrAddress() {
        return vlrAddress;
    }

    public void setVlrAddress(String vlrAddress) {
        this.vlrAddress = vlrAddress;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "SampleRequest{" +
                "message='" + message + '\'' +
                ", ussdOperation='" + ussdOperation + '\'' +
                ", requestId='" + requestId + '\'' +
                ", vlrAddress='" + vlrAddress + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", encoding='" + encoding + '\'' +
                ", sourceAddress='" + sourceAddress + '\'' +
                ", applicationId='" + applicationId + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
