package io.apptizer.tap.ussd.session;

/**
 * Created by isuru on 3/15/2017.
 */
public class SessionManagerConfigs {

    private String backInput;
    private String exitInput;
    private String paggingButton;
    private int sessionExpiryInSeconds;
    private String serviceCode;
    private String businessIdPrefix;
    private int msgChunkSize;

    public SessionManagerConfigs(String backInput,
                                 String exitInput,
                                 String paggingButton,
                                 int sessionExpiryInSeconds,
                                 String serviceCode,
                                 String businessIdPrefix,
                                 int msgChunkSize) {
        this.backInput = backInput;
        this.exitInput = exitInput;
        this.paggingButton = paggingButton;
        this.sessionExpiryInSeconds = sessionExpiryInSeconds;
        this.serviceCode = serviceCode;
        this.businessIdPrefix = businessIdPrefix;
        this.msgChunkSize=msgChunkSize;
    }

    public SessionManagerConfigs() {
    }

    public String getBackInput() {
        return backInput;
    }

    public void setBackInput(String backInput) {
        this.backInput = backInput;
    }

    public String getExitInput() {
        return exitInput;
    }

    public void setExitInput(String exitInput) {
        this.exitInput = exitInput;
    }

    public int getSessionExpiryInSeconds() {
        return sessionExpiryInSeconds;
    }

    public void setSessionExpiryInSeconds(int sessionExpiryInSeconds) {
        this.sessionExpiryInSeconds = sessionExpiryInSeconds;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getBusinessIdPrefix() {
        return businessIdPrefix;
    }

    public void setBusinessIdPrefix(String businessIdPrefix) {
        this.businessIdPrefix = businessIdPrefix;
    }

    public int getMsgChunkSize() {
        return msgChunkSize;
    }

    public void setMsgChunkSize(int msgChunkSize) {
        this.msgChunkSize = msgChunkSize;
    }

    public String getPaggingButton() {
        return paggingButton;
    }

    public void setPaggingButton(String paggingButton) {
        this.paggingButton = paggingButton;
    }
}
