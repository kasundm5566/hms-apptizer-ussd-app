package io.apptizer.tap.ussd.core.menus.templates;

import com.google.common.base.Strings;
import io.apptizer.api.model.merchant.BusinessSummary;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.BusinessService;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 4/4/17.
 */
@Component
@Qualifier("index")
@Primary
public class IndexMenuTemplate extends AbstractMenuTemplate {

    private final MenuResolver menuResolver;
    private final BusinessService businessService;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public IndexMenuTemplate(MenuResolver menuResolver,
                             BusinessService businessService,
                             MessageReader messageReader,
                             UssdContentRenderer ussdContentRenderer) {
        super("indexMenu", menuResolver);
        this.menuResolver = menuResolver;
        this.businessService = businessService;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        String businessName = session.getContext().get(KeyBox.BUSINESS_NAME + input);
        session.getContext().put(KeyBox.SELECTED_BUSINESS_NAME, businessName);
        String appId = session.getContext().get(KeyBox.BUSINESS_ID + input);
        if (Strings.isNullOrEmpty(appId)) {
            return menuResolver.getMenu("invalidInputMenu");
        }
        session.getContext().put(KeyBox.SELECTED_BUSINESS_ID, appId);
        return menuResolver.getMenu("welcomeMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        String caption = messageReader.readMessage("select.service", new Object[]{});
        List<BusinessSummary> businessSummaries = businessService.getAllBusinessesTap();
        Map<Integer, String> menuItems = new HashMap<>();
        int businessIdNumeric = 0;
        for (int i = 0; i < businessSummaries.size(); i++) {
            businessIdNumeric = Integer.parseInt(businessSummaries.get(i).getBusinessId().replaceAll("[^0-9]", ""));
            session.getContext().put(KeyBox.BUSINESS_NAME + businessIdNumeric, businessSummaries.get(i).getBusinessName());
            session.getContext().put(KeyBox.BUSINESS_ID + businessIdNumeric, businessSummaries.get(i).getBusinessId());
            menuItems.put(businessIdNumeric, businessSummaries.get(i).getBusinessName());
        }
        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(caption);
        mtContent.setItems(menuItems);
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

/*    @Override
    public UssdResponse content(Session session) {
        List<Business> businessList = businessService.getAllBusinesses();
        String message = messageReader.readMessage("select.service", new Object[]{});
        for (int i = 0; i < businessList.size(); i++) {
            message += (i + 1) + ". " + businessList.get(i).getName() + "\n";
            session.getContext().put(KeyBox.BUSINESS_NAME + (i + 1), businessList.get(i).getName());
        }
        message += messageReader.readMessage("exit.operation", new Object[]{});
        return new UssdResponse(message, UssdMtType.MT_CONT);
    }*/

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "IndexMenu";
    }
}
