package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.product.AddOn;
import io.apptizer.api.model.product.AddOnType;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.ProductService;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dinesh on 3/24/17.
 */

@Component
public class AddOnSelectMenuTemplate extends AbstractMenuTemplate {

    private final ProductService productService;
    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public AddOnSelectMenuTemplate(ProductService productService,
                                   MenuResolver menuResolver,
                                   MessageReader messageReader,
                                   UssdContentRenderer ussdContentRenderer) {
        super("addOnSelectMenu", menuResolver);
        this.productService = productService;
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        String productId = session.getContext().get(KeyBox.SELECTED_PRODUCT_ID);
        List<AddOn> addOnList = productService.getProductAddOns(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), productId);

        int addonMenuNo = Integer.parseInt(session.getContext().get(KeyBox.ADDON_ITEM_NO));
        addonMenuNo++;
        session.getContext().put(KeyBox.ADDON_ITEM_NO, String.valueOf(addonMenuNo));

        String addOn = session.getContext().get(KeyBox.ADDON_ID + input);
        String addOnName = session.getContext().get(KeyBox.ADDON_NAME + input);

        if (!(addOnName.equals("No") || addOnName.equals("None"))) {
            if (session.getContext().containsKey(KeyBox.SELECTED_ADDON_IDS)) {
                String existingAddons = session.getContext().get(KeyBox.SELECTED_ADDON_IDS);
                session.getContext().put(KeyBox.SELECTED_ADDON_IDS, existingAddons + "," + addOn);
            } else {
                session.getContext().put(KeyBox.SELECTED_ADDON_IDS, "");
                session.getContext().put(KeyBox.SELECTED_ADDON_IDS, addOn);
            }
        }

        for (int i = addonMenuNo; i < addOnList.size(); i++) {
            return this;
        }
        return menuResolver.getMenu("quantityMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        String productId = session.getContext().get(KeyBox.SELECTED_PRODUCT_ID);
        List<AddOn> addOnList = productService.getProductAddOns(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID), productId);
        int addonMenuNo = Integer.parseInt(session.getContext().get(KeyBox.ADDON_ITEM_NO));

        List<AddOnType> addOnTypeList = addOnList.get(addonMenuNo).getTypes();

        Map<Integer, String> optionsMap = new HashMap<>();
        if (addOnTypeList.size() == 1) {
            String option1 = messageReader.readMessage("single.addon.option1", new Object[]{});
            String option2 = messageReader.readMessage("single.addon.option2", new Object[]{});
            optionsMap.put(1, option1);
            optionsMap.put(2, option2);
            session.getContext().put(KeyBox.ADDON_ID1, addOnTypeList.get(0).getSubTypes().get(0).getSku());
            session.getContext().put(KeyBox.ADDON_NAME1, "Yes");
            session.getContext().put(KeyBox.ADDON_NAME2, "No");
        } else {
            for (int j = 0; j < addOnTypeList.size(); j++) {
                optionsMap.put(j + 1, addOnTypeList.get(j).getName());
                session.getContext().put(KeyBox.ADDON_ID + (j + 1), addOnTypeList.get(j).getSubTypes().get(0).getSku());
                session.getContext().put(KeyBox.ADDON_NAME + (j + 1), addOnTypeList.get(j).getName());
            }
        }

        String menuCaption = messageReader.readMessage("addon.choose", new Object[]{addOnList.get(addonMenuNo).getName()});

        UssdResponse.MtContent mtContent = new UssdResponse.MtContent();
        mtContent.setCaption(menuCaption);
        mtContent.setItems(optionsMap);

        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    @Override
    public boolean isBackSupported() {
        return false;
    }

    public String toString() {
        return "AddOnSelectMenu";
    }
}
