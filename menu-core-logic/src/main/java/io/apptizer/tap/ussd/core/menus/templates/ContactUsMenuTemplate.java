package io.apptizer.tap.ussd.core.menus.templates;

import io.apptizer.api.model.merchant.BusinessSummary;
import io.apptizer.tap.ussd.core.menus.config.KeyBox;
import io.apptizer.tap.ussd.core.menus.services.BusinessService;
import io.apptizer.tap.ussd.core.menus.services.MenuResolver;
import io.apptizer.tap.ussd.core.menus.services.MessageReader;
import io.apptizer.tap.ussd.core.menus.services.UssdContentRenderer;
import io.apptizer.tap.ussd.menu.MenuTemplate;
import io.apptizer.tap.ussd.messages.UssdMtType;
import io.apptizer.tap.ussd.messages.UssdResponse;
import io.apptizer.tap.ussd.messages.UssdResponseType;
import io.apptizer.tap.ussd.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by dinesh on 3/21/17.
 */

@Component
public class ContactUsMenuTemplate extends AbstractMenuTemplate {

    private final BusinessService businessService;
    private final MenuResolver menuResolver;
    private final MessageReader messageReader;
    private final UssdContentRenderer ussdContentRenderer;

    @Autowired
    public ContactUsMenuTemplate(BusinessService businessService,
                                 MenuResolver menuResolver,
                                 MessageReader messageReader,
                                 UssdContentRenderer ussdContentRenderer) {
        super("contactUsMenu", menuResolver);
        this.businessService = businessService;
        this.menuResolver = menuResolver;
        this.messageReader = messageReader;
        this.ussdContentRenderer = ussdContentRenderer;
    }

    @Override
    public MenuTemplate next(String input, Session session) {
        return menuResolver.getMenu("invalidInputMenu");
    }

    @Override
    public UssdResponse content(Session session) {
        BusinessSummary businessSummary = businessService.getBusinessData(session.getContext().get(KeyBox.SELECTED_BUSINESS_ID));
        String businessName = businessSummary.getBusinessName();
        String businessAddress = businessSummary.getAddress();
        String contactNo = businessSummary.getContactNumbers().get(0);

        String businessDetailsMsg = businessName + "<br/>" + businessAddress + "<br/>" + contactNo;
        String message = messageReader.readMessage("contact.details", new Object[]{businessDetailsMsg});

        UssdResponse.MtContent mtContent=new UssdResponse.MtContent();
        mtContent.setCaption(message);
        mtContent.setItems(new HashMap<>());
        return new UssdResponse(mtContent, UssdMtType.MT_CONT, UssdResponseType.STATIC);
    }

    public String toString() {
        return "ContactUsMenu";
    }
}
