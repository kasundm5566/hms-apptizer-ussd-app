package io.apptizer.tap.ussd.core.menus.services;

import io.apptizer.tap.ussd.menu.MenuTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dinesh on 3/27/17.
 */
@Component
public class MenuResolver {
    private final Map<String, MenuTemplate> menuTemplateMap = new HashMap<>();

    public void register(String id, MenuTemplate template) {
        menuTemplateMap.put(id, template);
    }

    public MenuTemplate getMenu(String id) {
        return menuTemplateMap.get(id);
    }
}
